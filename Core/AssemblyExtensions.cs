using System;
using System.Linq;
using System.Reflection;

namespace Core
{
	public static class AssemblyExtensions
	{
		
		public static Type GetTypeByClassName(this Assembly assembly, string className)
		{
			Type type = null;

			type = assembly.GetTypes().FirstOrDefault(x => x.Name == className);
            
			return type;
		}

	}
}