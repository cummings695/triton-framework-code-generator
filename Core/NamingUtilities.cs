using Castle.ActiveRecord.Framework.Internal;

namespace Core
{
	public class NamingUtilities
	{
		public static string SplitUpName(string name, char split)
		{
			string converted = "" + name[0];

			foreach (char c in name.Substring(1))
			{
				if (char.IsUpper(c))
				{
					converted += split;
				}
				converted += c;
			}
			return converted;
		}

		public static string UpdateWithUnderscores(string name)
		{
			return SplitUpName(name, '_');
		}

		public static string Capitalize(string str)
		{
			string retStr = null;

			if (str != null)
			{
				retStr = Inflector.Capitalize(str);
			}

			return retStr;
		}

		public static string DeCapitalize(string str)
		{
			string retStr = "";

			if (str != null)
			{
				retStr = char.ToLower(str[0]) + str.Substring(1);
			}

			return retStr;
		}

		public static string Pluralize(string single)
		{
			string retStr = null;

			if (single != null)
			{
				retStr = Inflector.Pluralize(single);
			}

			return retStr;
		}

		public static string Singularize(string multiple)
		{
			string retStr = null;

			if (multiple != null)
			{
				retStr = Inflector.Singularize(multiple);
			}
			if (string.IsNullOrEmpty(retStr))
			{
				retStr = multiple;
			}

			return retStr;
		}

		public static string GetVariableName(string name)
		{
			string retValue = DeCapitalize(name);

			Microsoft.CSharp.CSharpCodeProvider prov = new Microsoft.CSharp.CSharpCodeProvider();
			if (!prov.IsValidIdentifier(retValue)) {
				retValue = retValue + "Var";
			}

			return retValue; 
		}

		public static string GetPluralVariableName(string name)
		{
			return Pluralize(DeCapitalize(name));
		}

		public static string GetConstantName(string name)
		{
			return UpdateWithUnderscores(name).ToUpper();
		}

		public static string GetStringName(string name)
		{
			return SplitUpName(name, ' ');
		}
	}
}