﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using Castle.ActiveRecord.Framework.Internal;

namespace Core
{
	public static class Utilities
	{
		public static List<string> GetTypeNames(
			Type[] types,
			string filterText)
		{
			var typenames = from type in types
			                orderby type.FullName
			                where type.Namespace != null && type.Namespace.StartsWith(filterText)
			                select type.FullName;

			return typenames.ToList();
		}


		public static AutoCompleteStringCollection GetAutoComplete(Type[] types)
		{
			List<string> namespaces = (from type in types
			                           select type.Namespace).Distinct().ToList();

			AutoCompleteStringCollection autoCompleteList = new AutoCompleteStringCollection();

			foreach (string ns in namespaces) {
				if (!string.IsNullOrEmpty(ns)) {
					string[] split = ns.Split('.');
					AddNameSpace(split, 0, ref autoCompleteList);
				}
			}

			return autoCompleteList;
		}


		public static void AddNameSpace(
			string[] split,
			int counter,
			ref AutoCompleteStringCollection autoCompleteList)
		{
			if (counter < split.Length) {
				string addition = "";
				for (int i = 0; i <= counter; i++) {
					addition += split[i] + ".";
				}

				addition = addition.Remove(addition.Length - 1);

				if (!autoCompleteList.Contains(addition)) {
					autoCompleteList.Add(addition);
				}

				if (counter < split.Length) {
					AddNameSpace(split, counter + 1, ref autoCompleteList);
				}
			}
		}


		public static string GetBaseNameSpace(string modelNameSpace)
		{
			return modelNameSpace.Remove(modelNameSpace.IndexOf(".Model"));
		}


		/// <summary>
		/// 	for the sake of being consistent.
		/// </summary>
		/// <param name = "modelNameSpace"></param>
		/// <returns></returns>
		public static string GetModelNameSpace(string modelNameSpace)
		{
			return GetBaseNameSpace(modelNameSpace) + ".Model";
		}


		public static string GetSupportNameSpace(string modelNameSpace)
		{
			return GetBaseNameSpace(modelNameSpace) + ".Support";
		}


		public static string GetSupportRequestNameSpace(string modelNameSpace)
		{
			return GetSupportNameSpace(modelNameSpace) + ".Request";
		}


		public static string GetLogicNameSpace(string modelNameSpace)
		{
			return GetBaseNameSpace(modelNameSpace) + ".Logic";
		}


		public static string GetDaoNameSpace(string modelNameSpace)
		{
			return GetModelNameSpace(modelNameSpace) + ".Dao";
		}


		public static string GetFilterNameSpace(string modelNameSpace)
		{
			return GetDaoNameSpace(modelNameSpace);
		}


		public static string GetDaoSupportNameSpace(string modelNameSpace)
		{
			return GetDaoNameSpace(modelNameSpace) + ".Support";
		}

		public static string GetServiceNameSpace(string modelNameSpace)
		{
			return GetBaseNameSpace(modelNameSpace) + ".Service";
		}

		public static string GetServiceMessageNameSpace(string modelNameSpace)
		{
			return GetServiceNameSpace(modelNameSpace) + ".Message";
		}


		public static string GetServiceMapperNameSpace(string modelNameSpace)
		{
			return GetServiceNameSpace(modelNameSpace) + ".MessageMapper";
		}

		public static string GetServiceFilterMessageNameSpace(string modelNameSpace)
		{
			return GetServiceMessageNameSpace(modelNameSpace);
		}

		public static Type GetRealType(Type t)
		{
			Type toReturn = t;
			if (t.Name.StartsWith("Nullable")) {
				toReturn = Nullable.GetUnderlyingType(t);
			}

			return toReturn;
		}


		public static Type GetRealTypeFromMemberProperty(MemberInfo member)
		{
			return GetRealType(((PropertyInfo) member).PropertyType);
		}

		public static string GetTypeName(Type type)
		{
			return GetTypeName(type, "");
		}


		private static string GetTypeName(Type type, string appendString)
		{
			string name = type.Name;

			if (type.IsGenericType) {
				Type[] genTypes = type.GetGenericArguments();
				// remove the `1 or `2 from the generic type
				name = name.Remove(name.Length - 2);
				name += "<";
				
				//populate the name with the generic types.
				foreach (Type genType in genTypes) {
					name += GetTypeName(genType) + ",";
				}

				name = name.Remove(name.Length - 1);
				name += ">";
			} else {
				if (!IsPrimitive(type)) {
					name += appendString;
				}
			}

			return name;
		}

		public static string GetMessageTypeName(Type type)
		{
			return GetTypeName(type, "Message");
		}


		public static bool IsPrimitive(Type type)
		{
			return type == typeof (int) || type == typeof (long) ||
			       type == typeof (Guid) || type == typeof (string) ||
			       type == typeof (DateTime) || type == typeof (byte) ||
			       type == typeof (char) || type == typeof (float) ||
			       type == typeof (double) || type == typeof (bool) || type == typeof (decimal);
		}

		public static bool IsEnum(MemberInfo member)
		{
			return GetRealTypeFromMemberProperty(member).IsEnum;
		}

		public static bool IsList(Type type)
		{
			return type.Name == typeof (IList<object>).Name;
		}

		public static bool IsDictionary(Type type)
		{
			return type.Name == typeof (IDictionary<object, object>).Name;
		}


		public static string GetCommonNameForType(string type)
		{
			string retValue;

			switch (type) {
				case "Int32":
					retValue = "int";
					break;
				case "Int64":
					retValue = "long";
					break;
				case "Single":
					retValue = "float";
					break;
				case "Double":
					retValue = "double";
					break;
				case "Boolean":
					retValue = "bool";
					break;
				case "String":
					retValue = "string";
					break;
				default:
					retValue = type;
					break;
			}

			return retValue;
		}



	}
}