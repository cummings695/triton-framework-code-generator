using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Core;

namespace NHibernateGenerator
{
	public class DaoConfigGenerator
	{
		private const string DIRECTORY = @"GeneratedFiles\";

		public DaoConfigGenerator()
		{
		}

		public DaoConfigGenerator(Assembly assembly)
		{
			this.Assembly = assembly;
		}


		public Assembly Assembly { get; set; }


		public void Execute(IEnumerable<string> selectedClassNames)
		{
			string template = @"<dao name=""I{CLASS_NAME}Dao"" defaultClass=""nh"">
	<classes>
		<class name=""nh"" type=""{DAO_NAMESPACE}.Nh{CLASS_NAME}Dao,{ASSEMBLY}"" />
	</classes>
</dao>";

			string overallConfig = "";

			TextWriter writer;

			foreach (string fullName in selectedClassNames) {
				//create a new template string
				string classTemplate = template;

				Type classType = this.Assembly.GetType(fullName);
				string className = classType.Name;

				classTemplate = classTemplate.Replace("{CLASS_NAME}", className);
				classTemplate = classTemplate.Replace("{ASSEMBLY}", this.Assembly.GetName().Name);
				classTemplate = classTemplate.Replace("{DAO_NAMESPACE}", Utilities.GetDaoNameSpace(classType.Namespace));

				overallConfig += Environment.NewLine + classTemplate;
			}

			Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + DIRECTORY);

			writer = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + DIRECTORY + "dao.config.addition.xml");

			writer.Write(overallConfig);

			writer.Flush();
			writer.Close();
		}
	}
}