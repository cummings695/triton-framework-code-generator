﻿namespace GenerationSelector
{
	partial class MainSelector
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainSelector));
			this.dllSelectorDialogBox = new System.Windows.Forms.OpenFileDialog();
			this.classNameListingListBox = new System.Windows.Forms.ListBox();
			this.selectDllButton = new System.Windows.Forms.Button();
			this.selectedDllTextBox = new System.Windows.Forms.TextBox();
			this.nhibernateMappingFilesCheckBox = new System.Windows.Forms.CheckBox();
			this.generateButton = new System.Windows.Forms.Button();
			this.nhibernateDaoCheckBox = new System.Windows.Forms.CheckBox();
			this.daoInterfaceCheckBox = new System.Windows.Forms.CheckBox();
			this.filterFileCheckBox = new System.Windows.Forms.CheckBox();
			this.parameterNamesCheckBox = new System.Windows.Forms.CheckBox();
			this.itemNamesCheckBox = new System.Windows.Forms.CheckBox();
			this.deserializeCheckBox = new System.Windows.Forms.CheckBox();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.daoFactoryConfigCheckBox = new System.Windows.Forms.CheckBox();
			this.useIdBagCheckBox = new System.Windows.Forms.CheckBox();
			this.nhibernateConfigInsertCheckBox = new System.Windows.Forms.CheckBox();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.tabPage3 = new System.Windows.Forms.TabPage();
			this.populateActionCheckBox = new System.Windows.Forms.CheckBox();
			this.createActionCheckBox = new System.Windows.Forms.CheckBox();
			this.saveActionCheckBox = new System.Windows.Forms.CheckBox();
			this.deleteActionCheckBox = new System.Windows.Forms.CheckBox();
			this.getActionCheckBox = new System.Windows.Forms.CheckBox();
			this.tabPage4 = new System.Windows.Forms.TabPage();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.pagesSiteTextBox = new System.Windows.Forms.TextBox();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.refreshStartStateButton = new System.Windows.Forms.Button();
			this.startStateTextBox = new System.Windows.Forms.TextBox();
			this.pagesCheckBox = new System.Windows.Forms.CheckBox();
			this.validatorCheckBox = new System.Windows.Forms.CheckBox();
			this.rolesCheckBox = new System.Windows.Forms.CheckBox();
			this.statesConfigCheckBox = new System.Windows.Forms.CheckBox();
			this.tabPage5 = new System.Windows.Forms.TabPage();
			this.serviceFilterMessageCheckBox = new System.Windows.Forms.CheckBox();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.serviceRefreshButton = new System.Windows.Forms.Button();
			this.serviceStartStateTextBox = new System.Windows.Forms.TextBox();
			this.serviceStatesCheckBox = new System.Windows.Forms.CheckBox();
			this.controllerCheckBox = new System.Windows.Forms.CheckBox();
			this.serviceImplementationCheckBox = new System.Windows.Forms.CheckBox();
			this.serviceInterfaceCheckBox = new System.Windows.Forms.CheckBox();
			this.mapperCheckBox = new System.Windows.Forms.CheckBox();
			this.messageCheckBox = new System.Windows.Forms.CheckBox();
			this.namespaceFilterTextBox = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.openOutputButton = new System.Windows.Forms.Button();
			this.selectAllButton = new System.Windows.Forms.Button();
			this.clearAllButton = new System.Windows.Forms.Button();
			this.relationshipsButton = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.registerShellButton = new System.Windows.Forms.Button();
			this.unRegsiterShellButton = new System.Windows.Forms.Button();
			this.tabControl1.SuspendLayout();
			this.tabPage1.SuspendLayout();
			this.tabPage2.SuspendLayout();
			this.tabPage3.SuspendLayout();
			this.tabPage4.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.tabPage5.SuspendLayout();
			this.groupBox3.SuspendLayout();
			this.SuspendLayout();
			// 
			// dllSelectorDialogBox
			// 
			this.dllSelectorDialogBox.Filter = "DLLs|*.dll";
			this.dllSelectorDialogBox.FileOk += new System.ComponentModel.CancelEventHandler(this.dllSelectorDialogBox_FileOk);
			// 
			// classNameListingListBox
			// 
			this.classNameListingListBox.FormattingEnabled = true;
			this.classNameListingListBox.ItemHeight = 16;
			this.classNameListingListBox.Location = new System.Drawing.Point(20, 82);
			this.classNameListingListBox.Margin = new System.Windows.Forms.Padding(4);
			this.classNameListingListBox.Name = "classNameListingListBox";
			this.classNameListingListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
			this.classNameListingListBox.Size = new System.Drawing.Size(581, 132);
			this.classNameListingListBox.TabIndex = 0;
			// 
			// selectDllButton
			// 
			this.selectDllButton.Location = new System.Drawing.Point(470, 12);
			this.selectDllButton.Margin = new System.Windows.Forms.Padding(4);
			this.selectDllButton.Name = "selectDllButton";
			this.selectDllButton.Size = new System.Drawing.Size(129, 28);
			this.selectDllButton.TabIndex = 1;
			this.selectDllButton.Text = "Select Library";
			this.selectDllButton.UseVisualStyleBackColor = true;
			this.selectDllButton.Click += new System.EventHandler(this.selectDllButton_Click);
			// 
			// selectedDllTextBox
			// 
			this.selectedDllTextBox.Location = new System.Drawing.Point(20, 15);
			this.selectedDllTextBox.Margin = new System.Windows.Forms.Padding(4);
			this.selectedDllTextBox.Name = "selectedDllTextBox";
			this.selectedDllTextBox.Size = new System.Drawing.Size(444, 22);
			this.selectedDllTextBox.TabIndex = 2;
			// 
			// nhibernateMappingFilesCheckBox
			// 
			this.nhibernateMappingFilesCheckBox.AutoSize = true;
			this.nhibernateMappingFilesCheckBox.Checked = true;
			this.nhibernateMappingFilesCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.nhibernateMappingFilesCheckBox.Location = new System.Drawing.Point(8, 7);
			this.nhibernateMappingFilesCheckBox.Margin = new System.Windows.Forms.Padding(4);
			this.nhibernateMappingFilesCheckBox.Name = "nhibernateMappingFilesCheckBox";
			this.nhibernateMappingFilesCheckBox.Size = new System.Drawing.Size(193, 21);
			this.nhibernateMappingFilesCheckBox.TabIndex = 3;
			this.nhibernateMappingFilesCheckBox.Text = "NHibernate Mapping Files";
			this.nhibernateMappingFilesCheckBox.UseVisualStyleBackColor = true;
			// 
			// generateButton
			// 
			this.generateButton.Location = new System.Drawing.Point(497, 406);
			this.generateButton.Margin = new System.Windows.Forms.Padding(4);
			this.generateButton.Name = "generateButton";
			this.generateButton.Size = new System.Drawing.Size(100, 28);
			this.generateButton.TabIndex = 4;
			this.generateButton.Text = "Generate";
			this.generateButton.UseVisualStyleBackColor = true;
			this.generateButton.Click += new System.EventHandler(this.generateButton_Click);
			// 
			// nhibernateDaoCheckBox
			// 
			this.nhibernateDaoCheckBox.AutoSize = true;
			this.nhibernateDaoCheckBox.Checked = true;
			this.nhibernateDaoCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.nhibernateDaoCheckBox.Location = new System.Drawing.Point(8, 32);
			this.nhibernateDaoCheckBox.Margin = new System.Windows.Forms.Padding(4);
			this.nhibernateDaoCheckBox.Name = "nhibernateDaoCheckBox";
			this.nhibernateDaoCheckBox.Size = new System.Drawing.Size(143, 21);
			this.nhibernateDaoCheckBox.TabIndex = 5;
			this.nhibernateDaoCheckBox.Text = "NHibernate DAOs";
			this.nhibernateDaoCheckBox.UseVisualStyleBackColor = true;
			// 
			// daoInterfaceCheckBox
			// 
			this.daoInterfaceCheckBox.AutoSize = true;
			this.daoInterfaceCheckBox.Checked = true;
			this.daoInterfaceCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.daoInterfaceCheckBox.Location = new System.Drawing.Point(8, 60);
			this.daoInterfaceCheckBox.Margin = new System.Windows.Forms.Padding(4);
			this.daoInterfaceCheckBox.Name = "daoInterfaceCheckBox";
			this.daoInterfaceCheckBox.Size = new System.Drawing.Size(126, 21);
			this.daoInterfaceCheckBox.TabIndex = 6;
			this.daoInterfaceCheckBox.Text = "DAO Interfaces";
			this.daoInterfaceCheckBox.UseVisualStyleBackColor = true;
			// 
			// filterFileCheckBox
			// 
			this.filterFileCheckBox.AutoSize = true;
			this.filterFileCheckBox.Checked = true;
			this.filterFileCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.filterFileCheckBox.Location = new System.Drawing.Point(8, 89);
			this.filterFileCheckBox.Margin = new System.Windows.Forms.Padding(4);
			this.filterFileCheckBox.Name = "filterFileCheckBox";
			this.filterFileCheckBox.Size = new System.Drawing.Size(94, 21);
			this.filterFileCheckBox.TabIndex = 7;
			this.filterFileCheckBox.Text = "Filter Files";
			this.filterFileCheckBox.UseVisualStyleBackColor = true;
			// 
			// parameterNamesCheckBox
			// 
			this.parameterNamesCheckBox.AutoSize = true;
			this.parameterNamesCheckBox.Checked = true;
			this.parameterNamesCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.parameterNamesCheckBox.Location = new System.Drawing.Point(8, 36);
			this.parameterNamesCheckBox.Margin = new System.Windows.Forms.Padding(4);
			this.parameterNamesCheckBox.Name = "parameterNamesCheckBox";
			this.parameterNamesCheckBox.Size = new System.Drawing.Size(144, 21);
			this.parameterNamesCheckBox.TabIndex = 8;
			this.parameterNamesCheckBox.Text = "Parameter Names";
			this.parameterNamesCheckBox.UseVisualStyleBackColor = true;
			// 
			// itemNamesCheckBox
			// 
			this.itemNamesCheckBox.AutoSize = true;
			this.itemNamesCheckBox.Checked = true;
			this.itemNamesCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.itemNamesCheckBox.Location = new System.Drawing.Point(8, 63);
			this.itemNamesCheckBox.Margin = new System.Windows.Forms.Padding(4);
			this.itemNamesCheckBox.Name = "itemNamesCheckBox";
			this.itemNamesCheckBox.Size = new System.Drawing.Size(104, 21);
			this.itemNamesCheckBox.TabIndex = 9;
			this.itemNamesCheckBox.Text = "Item Names";
			this.itemNamesCheckBox.UseVisualStyleBackColor = true;
			// 
			// deserializeCheckBox
			// 
			this.deserializeCheckBox.AutoSize = true;
			this.deserializeCheckBox.Checked = true;
			this.deserializeCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.deserializeCheckBox.Location = new System.Drawing.Point(8, 7);
			this.deserializeCheckBox.Margin = new System.Windows.Forms.Padding(4);
			this.deserializeCheckBox.Name = "deserializeCheckBox";
			this.deserializeCheckBox.Size = new System.Drawing.Size(100, 21);
			this.deserializeCheckBox.TabIndex = 10;
			this.deserializeCheckBox.Text = "Deserialize";
			this.deserializeCheckBox.UseVisualStyleBackColor = true;
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Controls.Add(this.tabPage2);
			this.tabControl1.Controls.Add(this.tabPage3);
			this.tabControl1.Controls.Add(this.tabPage4);
			this.tabControl1.Controls.Add(this.tabPage5);
			this.tabControl1.Location = new System.Drawing.Point(20, 222);
			this.tabControl1.Margin = new System.Windows.Forms.Padding(4);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(583, 176);
			this.tabControl1.TabIndex = 11;
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.daoFactoryConfigCheckBox);
			this.tabPage1.Controls.Add(this.useIdBagCheckBox);
			this.tabPage1.Controls.Add(this.nhibernateConfigInsertCheckBox);
			this.tabPage1.Controls.Add(this.filterFileCheckBox);
			this.tabPage1.Controls.Add(this.nhibernateMappingFilesCheckBox);
			this.tabPage1.Controls.Add(this.nhibernateDaoCheckBox);
			this.tabPage1.Controls.Add(this.daoInterfaceCheckBox);
			this.tabPage1.Location = new System.Drawing.Point(4, 25);
			this.tabPage1.Margin = new System.Windows.Forms.Padding(4);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Padding = new System.Windows.Forms.Padding(4);
			this.tabPage1.Size = new System.Drawing.Size(575, 147);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "DAO";
			this.tabPage1.UseVisualStyleBackColor = true;
			// 
			// daoFactoryConfigCheckBox
			// 
			this.daoFactoryConfigCheckBox.AutoSize = true;
			this.daoFactoryConfigCheckBox.Checked = true;
			this.daoFactoryConfigCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.daoFactoryConfigCheckBox.Location = new System.Drawing.Point(201, 115);
			this.daoFactoryConfigCheckBox.Name = "daoFactoryConfigCheckBox";
			this.daoFactoryConfigCheckBox.Size = new System.Drawing.Size(194, 21);
			this.daoFactoryConfigCheckBox.TabIndex = 10;
			this.daoFactoryConfigCheckBox.Text = "DAO Factory Config Insert";
			this.daoFactoryConfigCheckBox.UseVisualStyleBackColor = true;
			// 
			// useIdBagCheckBox
			// 
			this.useIdBagCheckBox.AutoSize = true;
			this.useIdBagCheckBox.Checked = true;
			this.useIdBagCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.useIdBagCheckBox.Location = new System.Drawing.Point(199, 7);
			this.useIdBagCheckBox.Name = "useIdBagCheckBox";
			this.useIdBagCheckBox.Size = new System.Drawing.Size(109, 21);
			this.useIdBagCheckBox.TabIndex = 9;
			this.useIdBagCheckBox.Text = "Use ID Bag?";
			this.useIdBagCheckBox.UseVisualStyleBackColor = true;
			// 
			// nhibernateConfigInsertCheckBox
			// 
			this.nhibernateConfigInsertCheckBox.AutoSize = true;
			this.nhibernateConfigInsertCheckBox.Checked = true;
			this.nhibernateConfigInsertCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.nhibernateConfigInsertCheckBox.Location = new System.Drawing.Point(8, 116);
			this.nhibernateConfigInsertCheckBox.Margin = new System.Windows.Forms.Padding(4);
			this.nhibernateConfigInsertCheckBox.Name = "nhibernateConfigInsertCheckBox";
			this.nhibernateConfigInsertCheckBox.Size = new System.Drawing.Size(185, 21);
			this.nhibernateConfigInsertCheckBox.TabIndex = 8;
			this.nhibernateConfigInsertCheckBox.Text = "NHibernate Config Insert";
			this.nhibernateConfigInsertCheckBox.UseVisualStyleBackColor = true;
			// 
			// tabPage2
			// 
			this.tabPage2.Controls.Add(this.deserializeCheckBox);
			this.tabPage2.Controls.Add(this.parameterNamesCheckBox);
			this.tabPage2.Controls.Add(this.itemNamesCheckBox);
			this.tabPage2.Location = new System.Drawing.Point(4, 25);
			this.tabPage2.Margin = new System.Windows.Forms.Padding(4);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Padding = new System.Windows.Forms.Padding(4);
			this.tabPage2.Size = new System.Drawing.Size(575, 147);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "Support";
			this.tabPage2.UseVisualStyleBackColor = true;
			// 
			// tabPage3
			// 
			this.tabPage3.Controls.Add(this.populateActionCheckBox);
			this.tabPage3.Controls.Add(this.createActionCheckBox);
			this.tabPage3.Controls.Add(this.saveActionCheckBox);
			this.tabPage3.Controls.Add(this.deleteActionCheckBox);
			this.tabPage3.Controls.Add(this.getActionCheckBox);
			this.tabPage3.Location = new System.Drawing.Point(4, 25);
			this.tabPage3.Margin = new System.Windows.Forms.Padding(4);
			this.tabPage3.Name = "tabPage3";
			this.tabPage3.Size = new System.Drawing.Size(575, 147);
			this.tabPage3.TabIndex = 2;
			this.tabPage3.Text = "Logic";
			this.tabPage3.UseVisualStyleBackColor = true;
			// 
			// populateActionCheckBox
			// 
			this.populateActionCheckBox.AutoSize = true;
			this.populateActionCheckBox.Checked = true;
			this.populateActionCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.populateActionCheckBox.Location = new System.Drawing.Point(5, 119);
			this.populateActionCheckBox.Margin = new System.Windows.Forms.Padding(4);
			this.populateActionCheckBox.Name = "populateActionCheckBox";
			this.populateActionCheckBox.Size = new System.Drawing.Size(129, 21);
			this.populateActionCheckBox.TabIndex = 4;
			this.populateActionCheckBox.Text = "Populate Action";
			this.populateActionCheckBox.UseVisualStyleBackColor = true;
			// 
			// createActionCheckBox
			// 
			this.createActionCheckBox.AutoSize = true;
			this.createActionCheckBox.Checked = true;
			this.createActionCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.createActionCheckBox.Location = new System.Drawing.Point(5, 94);
			this.createActionCheckBox.Margin = new System.Windows.Forms.Padding(4);
			this.createActionCheckBox.Name = "createActionCheckBox";
			this.createActionCheckBox.Size = new System.Drawing.Size(200, 21);
			this.createActionCheckBox.TabIndex = 3;
			this.createActionCheckBox.Text = "CreateFromRequest Action";
			this.createActionCheckBox.UseVisualStyleBackColor = true;
			// 
			// saveActionCheckBox
			// 
			this.saveActionCheckBox.AutoSize = true;
			this.saveActionCheckBox.Checked = true;
			this.saveActionCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.saveActionCheckBox.Location = new System.Drawing.Point(5, 64);
			this.saveActionCheckBox.Margin = new System.Windows.Forms.Padding(4);
			this.saveActionCheckBox.Name = "saveActionCheckBox";
			this.saveActionCheckBox.Size = new System.Drawing.Size(105, 21);
			this.saveActionCheckBox.TabIndex = 2;
			this.saveActionCheckBox.Text = "Save Action";
			this.saveActionCheckBox.UseVisualStyleBackColor = true;
			// 
			// deleteActionCheckBox
			// 
			this.deleteActionCheckBox.AutoSize = true;
			this.deleteActionCheckBox.Checked = true;
			this.deleteActionCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.deleteActionCheckBox.Location = new System.Drawing.Point(5, 34);
			this.deleteActionCheckBox.Margin = new System.Windows.Forms.Padding(4);
			this.deleteActionCheckBox.Name = "deleteActionCheckBox";
			this.deleteActionCheckBox.Size = new System.Drawing.Size(114, 21);
			this.deleteActionCheckBox.TabIndex = 1;
			this.deleteActionCheckBox.Text = "Delete Action";
			this.deleteActionCheckBox.UseVisualStyleBackColor = true;
			// 
			// getActionCheckBox
			// 
			this.getActionCheckBox.AutoSize = true;
			this.getActionCheckBox.Checked = true;
			this.getActionCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.getActionCheckBox.Location = new System.Drawing.Point(5, 5);
			this.getActionCheckBox.Margin = new System.Windows.Forms.Padding(4);
			this.getActionCheckBox.Name = "getActionCheckBox";
			this.getActionCheckBox.Size = new System.Drawing.Size(96, 21);
			this.getActionCheckBox.TabIndex = 0;
			this.getActionCheckBox.Text = "Get Action";
			this.getActionCheckBox.UseVisualStyleBackColor = true;
			// 
			// tabPage4
			// 
			this.tabPage4.Controls.Add(this.groupBox2);
			this.tabPage4.Controls.Add(this.groupBox1);
			this.tabPage4.Controls.Add(this.pagesCheckBox);
			this.tabPage4.Controls.Add(this.validatorCheckBox);
			this.tabPage4.Controls.Add(this.rolesCheckBox);
			this.tabPage4.Controls.Add(this.statesConfigCheckBox);
			this.tabPage4.Location = new System.Drawing.Point(4, 25);
			this.tabPage4.Margin = new System.Windows.Forms.Padding(4);
			this.tabPage4.Name = "tabPage4";
			this.tabPage4.Padding = new System.Windows.Forms.Padding(4);
			this.tabPage4.Size = new System.Drawing.Size(575, 147);
			this.tabPage4.TabIndex = 3;
			this.tabPage4.Text = "Web";
			this.tabPage4.UseVisualStyleBackColor = true;
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.pagesSiteTextBox);
			this.groupBox2.Location = new System.Drawing.Point(75, 89);
			this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
			this.groupBox2.Size = new System.Drawing.Size(125, 50);
			this.groupBox2.TabIndex = 6;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Site";
			// 
			// pagesSiteTextBox
			// 
			this.pagesSiteTextBox.Location = new System.Drawing.Point(8, 18);
			this.pagesSiteTextBox.Margin = new System.Windows.Forms.Padding(4);
			this.pagesSiteTextBox.Name = "pagesSiteTextBox";
			this.pagesSiteTextBox.Size = new System.Drawing.Size(109, 22);
			this.pagesSiteTextBox.TabIndex = 4;
			this.pagesSiteTextBox.Text = "default";
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.refreshStartStateButton);
			this.groupBox1.Controls.Add(this.startStateTextBox);
			this.groupBox1.Location = new System.Drawing.Point(136, 7);
			this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
			this.groupBox1.Size = new System.Drawing.Size(125, 50);
			this.groupBox1.TabIndex = 5;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Start State";
			// 
			// refreshStartStateButton
			// 
			this.refreshStartStateButton.AccessibleDescription = "";
			this.refreshStartStateButton.Image = ((System.Drawing.Image)(resources.GetObject("refreshStartStateButton.Image")));
			this.refreshStartStateButton.Location = new System.Drawing.Point(89, 17);
			this.refreshStartStateButton.Name = "refreshStartStateButton";
			this.refreshStartStateButton.Size = new System.Drawing.Size(29, 23);
			this.refreshStartStateButton.TabIndex = 5;
			this.refreshStartStateButton.UseVisualStyleBackColor = true;
			this.refreshStartStateButton.Click += new System.EventHandler(this.refreshStartStateButton_Click);
			// 
			// startStateTextBox
			// 
			this.startStateTextBox.Location = new System.Drawing.Point(8, 18);
			this.startStateTextBox.Margin = new System.Windows.Forms.Padding(4);
			this.startStateTextBox.Name = "startStateTextBox";
			this.startStateTextBox.Size = new System.Drawing.Size(80, 22);
			this.startStateTextBox.TabIndex = 4;
			this.startStateTextBox.Text = "42";
			// 
			// pagesCheckBox
			// 
			this.pagesCheckBox.AutoSize = true;
			this.pagesCheckBox.Checked = true;
			this.pagesCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.pagesCheckBox.Location = new System.Drawing.Point(9, 97);
			this.pagesCheckBox.Margin = new System.Windows.Forms.Padding(4);
			this.pagesCheckBox.Name = "pagesCheckBox";
			this.pagesCheckBox.Size = new System.Drawing.Size(70, 21);
			this.pagesCheckBox.TabIndex = 3;
			this.pagesCheckBox.Text = "Pages";
			this.pagesCheckBox.UseVisualStyleBackColor = true;
			// 
			// validatorCheckBox
			// 
			this.validatorCheckBox.AutoSize = true;
			this.validatorCheckBox.Checked = true;
			this.validatorCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.validatorCheckBox.Location = new System.Drawing.Point(9, 69);
			this.validatorCheckBox.Margin = new System.Windows.Forms.Padding(4);
			this.validatorCheckBox.Name = "validatorCheckBox";
			this.validatorCheckBox.Size = new System.Drawing.Size(112, 21);
			this.validatorCheckBox.TabIndex = 2;
			this.validatorCheckBox.Text = "Validator File";
			this.validatorCheckBox.UseVisualStyleBackColor = true;
			// 
			// rolesCheckBox
			// 
			this.rolesCheckBox.AutoSize = true;
			this.rolesCheckBox.Checked = true;
			this.rolesCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.rolesCheckBox.Location = new System.Drawing.Point(9, 38);
			this.rolesCheckBox.Margin = new System.Windows.Forms.Padding(4);
			this.rolesCheckBox.Name = "rolesCheckBox";
			this.rolesCheckBox.Size = new System.Drawing.Size(97, 21);
			this.rolesCheckBox.TabIndex = 1;
			this.rolesCheckBox.Text = "Roles CSV";
			this.rolesCheckBox.UseVisualStyleBackColor = true;
			// 
			// statesConfigCheckBox
			// 
			this.statesConfigCheckBox.AutoSize = true;
			this.statesConfigCheckBox.Checked = true;
			this.statesConfigCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.statesConfigCheckBox.Location = new System.Drawing.Point(9, 9);
			this.statesConfigCheckBox.Margin = new System.Windows.Forms.Padding(4);
			this.statesConfigCheckBox.Name = "statesConfigCheckBox";
			this.statesConfigCheckBox.Size = new System.Drawing.Size(114, 21);
			this.statesConfigCheckBox.TabIndex = 0;
			this.statesConfigCheckBox.Text = "States Config";
			this.statesConfigCheckBox.UseVisualStyleBackColor = true;
			// 
			// tabPage5
			// 
			this.tabPage5.Controls.Add(this.serviceFilterMessageCheckBox);
			this.tabPage5.Controls.Add(this.groupBox3);
			this.tabPage5.Controls.Add(this.serviceStatesCheckBox);
			this.tabPage5.Controls.Add(this.controllerCheckBox);
			this.tabPage5.Controls.Add(this.serviceImplementationCheckBox);
			this.tabPage5.Controls.Add(this.serviceInterfaceCheckBox);
			this.tabPage5.Controls.Add(this.mapperCheckBox);
			this.tabPage5.Controls.Add(this.messageCheckBox);
			this.tabPage5.Location = new System.Drawing.Point(4, 25);
			this.tabPage5.Name = "tabPage5";
			this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage5.Size = new System.Drawing.Size(575, 147);
			this.tabPage5.TabIndex = 4;
			this.tabPage5.Text = "Services";
			this.tabPage5.UseVisualStyleBackColor = true;
			// 
			// serviceFilterMessageCheckBox
			// 
			this.serviceFilterMessageCheckBox.AutoSize = true;
			this.serviceFilterMessageCheckBox.Location = new System.Drawing.Point(6, 117);
			this.serviceFilterMessageCheckBox.Name = "serviceFilterMessageCheckBox";
			this.serviceFilterMessageCheckBox.Size = new System.Drawing.Size(122, 21);
			this.serviceFilterMessageCheckBox.TabIndex = 7;
			this.serviceFilterMessageCheckBox.Text = "Filter Message";
			this.serviceFilterMessageCheckBox.UseVisualStyleBackColor = true;
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.serviceRefreshButton);
			this.groupBox3.Controls.Add(this.serviceStartStateTextBox);
			this.groupBox3.Location = new System.Drawing.Point(256, 7);
			this.groupBox3.Margin = new System.Windows.Forms.Padding(4);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Padding = new System.Windows.Forms.Padding(4);
			this.groupBox3.Size = new System.Drawing.Size(125, 50);
			this.groupBox3.TabIndex = 6;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Start State";
			// 
			// serviceRefreshButton
			// 
			this.serviceRefreshButton.AccessibleDescription = "";
			this.serviceRefreshButton.Image = ((System.Drawing.Image)(resources.GetObject("serviceRefreshButton.Image")));
			this.serviceRefreshButton.Location = new System.Drawing.Point(89, 17);
			this.serviceRefreshButton.Name = "serviceRefreshButton";
			this.serviceRefreshButton.Size = new System.Drawing.Size(29, 23);
			this.serviceRefreshButton.TabIndex = 5;
			this.serviceRefreshButton.UseVisualStyleBackColor = true;
			this.serviceRefreshButton.Click += new System.EventHandler(this.serviceRefreshButton_Click);
			// 
			// serviceStartStateTextBox
			// 
			this.serviceStartStateTextBox.Location = new System.Drawing.Point(8, 18);
			this.serviceStartStateTextBox.Margin = new System.Windows.Forms.Padding(4);
			this.serviceStartStateTextBox.Name = "serviceStartStateTextBox";
			this.serviceStartStateTextBox.Size = new System.Drawing.Size(80, 22);
			this.serviceStartStateTextBox.TabIndex = 4;
			this.serviceStartStateTextBox.Text = "42";
			// 
			// serviceStatesCheckBox
			// 
			this.serviceStatesCheckBox.AutoSize = true;
			this.serviceStatesCheckBox.Location = new System.Drawing.Point(179, 7);
			this.serviceStatesCheckBox.Name = "serviceStatesCheckBox";
			this.serviceStatesCheckBox.Size = new System.Drawing.Size(70, 21);
			this.serviceStatesCheckBox.TabIndex = 5;
			this.serviceStatesCheckBox.Text = "States";
			this.serviceStatesCheckBox.UseVisualStyleBackColor = true;
			// 
			// controllerCheckBox
			// 
			this.controllerCheckBox.AutoSize = true;
			this.controllerCheckBox.Location = new System.Drawing.Point(179, 63);
			this.controllerCheckBox.Name = "controllerCheckBox";
			this.controllerCheckBox.Size = new System.Drawing.Size(91, 21);
			this.controllerCheckBox.TabIndex = 4;
			this.controllerCheckBox.Text = "Controller";
			this.controllerCheckBox.UseVisualStyleBackColor = true;
			// 
			// serviceImplementationCheckBox
			// 
			this.serviceImplementationCheckBox.AutoSize = true;
			this.serviceImplementationCheckBox.Location = new System.Drawing.Point(7, 90);
			this.serviceImplementationCheckBox.Name = "serviceImplementationCheckBox";
			this.serviceImplementationCheckBox.Size = new System.Drawing.Size(125, 21);
			this.serviceImplementationCheckBox.TabIndex = 3;
			this.serviceImplementationCheckBox.Text = "Implementation";
			this.serviceImplementationCheckBox.UseVisualStyleBackColor = true;
			// 
			// serviceInterfaceCheckBox
			// 
			this.serviceInterfaceCheckBox.AutoSize = true;
			this.serviceInterfaceCheckBox.Location = new System.Drawing.Point(7, 63);
			this.serviceInterfaceCheckBox.Name = "serviceInterfaceCheckBox";
			this.serviceInterfaceCheckBox.Size = new System.Drawing.Size(85, 21);
			this.serviceInterfaceCheckBox.TabIndex = 2;
			this.serviceInterfaceCheckBox.Text = "Interface";
			this.serviceInterfaceCheckBox.UseVisualStyleBackColor = true;
			// 
			// mapperCheckBox
			// 
			this.mapperCheckBox.AutoSize = true;
			this.mapperCheckBox.Location = new System.Drawing.Point(7, 35);
			this.mapperCheckBox.Name = "mapperCheckBox";
			this.mapperCheckBox.Size = new System.Drawing.Size(85, 21);
			this.mapperCheckBox.TabIndex = 1;
			this.mapperCheckBox.Text = "Mappers";
			this.mapperCheckBox.UseVisualStyleBackColor = true;
			// 
			// messageCheckBox
			// 
			this.messageCheckBox.AutoSize = true;
			this.messageCheckBox.Location = new System.Drawing.Point(7, 7);
			this.messageCheckBox.Name = "messageCheckBox";
			this.messageCheckBox.Size = new System.Drawing.Size(94, 21);
			this.messageCheckBox.TabIndex = 0;
			this.messageCheckBox.Text = "Messages";
			this.messageCheckBox.UseVisualStyleBackColor = true;
			// 
			// namespaceFilterTextBox
			// 
			this.namespaceFilterTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
			this.namespaceFilterTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
			this.namespaceFilterTextBox.Location = new System.Drawing.Point(139, 47);
			this.namespaceFilterTextBox.Margin = new System.Windows.Forms.Padding(4);
			this.namespaceFilterTextBox.Name = "namespaceFilterTextBox";
			this.namespaceFilterTextBox.Size = new System.Drawing.Size(325, 22);
			this.namespaceFilterTextBox.TabIndex = 12;
			this.namespaceFilterTextBox.TextChanged += new System.EventHandler(this.Namespace_TextChanged);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(20, 49);
			this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(118, 17);
			this.label1.TabIndex = 13;
			this.label1.Text = "Namespace Filter";
			// 
			// openOutputButton
			// 
			this.openOutputButton.Location = new System.Drawing.Point(470, 44);
			this.openOutputButton.Margin = new System.Windows.Forms.Padding(4);
			this.openOutputButton.Name = "openOutputButton";
			this.openOutputButton.Size = new System.Drawing.Size(131, 28);
			this.openOutputButton.TabIndex = 14;
			this.openOutputButton.Text = "Open Output";
			this.openOutputButton.UseVisualStyleBackColor = true;
			this.openOutputButton.Click += new System.EventHandler(this.openOutputButton_Click);
			// 
			// selectAllButton
			// 
			this.selectAllButton.Location = new System.Drawing.Point(16, 406);
			this.selectAllButton.Margin = new System.Windows.Forms.Padding(4);
			this.selectAllButton.Name = "selectAllButton";
			this.selectAllButton.Size = new System.Drawing.Size(100, 28);
			this.selectAllButton.TabIndex = 15;
			this.selectAllButton.Text = "Select All";
			this.selectAllButton.UseVisualStyleBackColor = true;
			this.selectAllButton.Click += new System.EventHandler(this.selectAllButton_Click);
			// 
			// clearAllButton
			// 
			this.clearAllButton.Location = new System.Drawing.Point(124, 406);
			this.clearAllButton.Margin = new System.Windows.Forms.Padding(4);
			this.clearAllButton.Name = "clearAllButton";
			this.clearAllButton.Size = new System.Drawing.Size(100, 28);
			this.clearAllButton.TabIndex = 16;
			this.clearAllButton.Text = "Clear All";
			this.clearAllButton.UseVisualStyleBackColor = true;
			this.clearAllButton.Click += new System.EventHandler(this.clearAllButton_Click);
			// 
			// relationshipsButton
			// 
			this.relationshipsButton.Location = new System.Drawing.Point(349, 406);
			this.relationshipsButton.Name = "relationshipsButton";
			this.relationshipsButton.Size = new System.Drawing.Size(141, 28);
			this.relationshipsButton.TabIndex = 5;
			this.relationshipsButton.Text = "Relationships";
			this.relationshipsButton.UseVisualStyleBackColor = true;
			this.relationshipsButton.Click += new System.EventHandler(this.relationshipsButton_Click);
			// 
			// button1
			// 
			this.button1.AccessibleDescription = "";
			this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
			this.button1.Location = new System.Drawing.Point(89, 17);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(29, 23);
			this.button1.TabIndex = 5;
			this.button1.UseVisualStyleBackColor = true;
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(8, 18);
			this.textBox1.Margin = new System.Windows.Forms.Padding(4);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(80, 22);
			this.textBox1.TabIndex = 4;
			this.textBox1.Text = "42";
			// 
			// registerShellButton
			// 
			this.registerShellButton.Location = new System.Drawing.Point(17, 441);
			this.registerShellButton.Name = "registerShellButton";
			this.registerShellButton.Size = new System.Drawing.Size(141, 28);
			this.registerShellButton.TabIndex = 17;
			this.registerShellButton.Text = "Register With Shell";
			this.registerShellButton.UseVisualStyleBackColor = true;
			this.registerShellButton.Click += new System.EventHandler(this.registerShellButton_Click);
			// 
			// unRegsiterShellButton
			// 
			this.unRegsiterShellButton.Location = new System.Drawing.Point(164, 441);
			this.unRegsiterShellButton.Name = "unRegsiterShellButton";
			this.unRegsiterShellButton.Size = new System.Drawing.Size(168, 28);
			this.unRegsiterShellButton.TabIndex = 18;
			this.unRegsiterShellButton.Text = "Unregister From Shell";
			this.unRegsiterShellButton.UseVisualStyleBackColor = true;
			this.unRegsiterShellButton.Click += new System.EventHandler(this.unRegsiterShellButton_Click);
			// 
			// MainSelector
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(619, 476);
			this.Controls.Add(this.unRegsiterShellButton);
			this.Controls.Add(this.registerShellButton);
			this.Controls.Add(this.relationshipsButton);
			this.Controls.Add(this.clearAllButton);
			this.Controls.Add(this.selectAllButton);
			this.Controls.Add(this.openOutputButton);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.namespaceFilterTextBox);
			this.Controls.Add(this.tabControl1);
			this.Controls.Add(this.generateButton);
			this.Controls.Add(this.selectedDllTextBox);
			this.Controls.Add(this.selectDllButton);
			this.Controls.Add(this.classNameListingListBox);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Margin = new System.Windows.Forms.Padding(4);
			this.Name = "MainSelector";
			this.Text = "Triton Code Generator";
			this.tabControl1.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			this.tabPage1.PerformLayout();
			this.tabPage2.ResumeLayout(false);
			this.tabPage2.PerformLayout();
			this.tabPage3.ResumeLayout(false);
			this.tabPage3.PerformLayout();
			this.tabPage4.ResumeLayout(false);
			this.tabPage4.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.tabPage5.ResumeLayout(false);
			this.tabPage5.PerformLayout();
			this.groupBox3.ResumeLayout(false);
			this.groupBox3.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.OpenFileDialog dllSelectorDialogBox;
		private System.Windows.Forms.ListBox classNameListingListBox;
		private System.Windows.Forms.Button selectDllButton;
		private System.Windows.Forms.TextBox selectedDllTextBox;
		private System.Windows.Forms.CheckBox nhibernateMappingFilesCheckBox;
		private System.Windows.Forms.Button generateButton;
		private System.Windows.Forms.CheckBox nhibernateDaoCheckBox;
		private System.Windows.Forms.CheckBox daoInterfaceCheckBox;
		private System.Windows.Forms.CheckBox filterFileCheckBox;
		private System.Windows.Forms.CheckBox parameterNamesCheckBox;
		private System.Windows.Forms.CheckBox itemNamesCheckBox;
		private System.Windows.Forms.CheckBox deserializeCheckBox;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.TabPage tabPage3;
		private System.Windows.Forms.CheckBox populateActionCheckBox;
		private System.Windows.Forms.CheckBox createActionCheckBox;
		private System.Windows.Forms.CheckBox saveActionCheckBox;
		private System.Windows.Forms.CheckBox deleteActionCheckBox;
		private System.Windows.Forms.CheckBox getActionCheckBox;
		private System.Windows.Forms.TabPage tabPage4;
		private System.Windows.Forms.CheckBox validatorCheckBox;
		private System.Windows.Forms.CheckBox rolesCheckBox;
		private System.Windows.Forms.CheckBox statesConfigCheckBox;
		private System.Windows.Forms.CheckBox pagesCheckBox;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.TextBox startStateTextBox;
		private System.Windows.Forms.TextBox namespaceFilterTextBox;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button openOutputButton;
		private System.Windows.Forms.Button selectAllButton;
		private System.Windows.Forms.Button clearAllButton;
		private System.Windows.Forms.CheckBox nhibernateConfigInsertCheckBox;
		private System.Windows.Forms.Button refreshStartStateButton;
		private System.Windows.Forms.CheckBox useIdBagCheckBox;
		private System.Windows.Forms.Button relationshipsButton;
		private System.Windows.Forms.CheckBox daoFactoryConfigCheckBox;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.TextBox pagesSiteTextBox;
		private System.Windows.Forms.TabPage tabPage5;
		private System.Windows.Forms.CheckBox mapperCheckBox;
		private System.Windows.Forms.CheckBox messageCheckBox;
		private System.Windows.Forms.CheckBox serviceImplementationCheckBox;
		private System.Windows.Forms.CheckBox serviceInterfaceCheckBox;
		private System.Windows.Forms.CheckBox controllerCheckBox;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.Button serviceRefreshButton;
		private System.Windows.Forms.TextBox serviceStartStateTextBox;
		private System.Windows.Forms.CheckBox serviceStatesCheckBox;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.CheckBox serviceFilterMessageCheckBox;
		private System.Windows.Forms.Button registerShellButton;
		private System.Windows.Forms.Button unRegsiterShellButton;
	}
}