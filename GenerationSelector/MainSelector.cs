﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using Core;
using DaoGenerator;
using LogicGenerator;
using Microsoft.Win32;
using NHibernateGenerator;
using ServicesGenerator;
using SupportGenerator;
using WebViewGenerator;

namespace GenerationSelector
{
	public partial class MainSelector : Form
	{
		private const string REGISTRY_PATH = @"dllfile\shell\Triton Code Generator";
		private Assembly assemblyInfo;

		public MainSelector() : this(null)
		{
		}

		public MainSelector(string initialDllPath)
		{
			this.InitializeComponent();

			this.startStateTextBox.Text = new Random().Next(10, 100).ToString();
			this.serviceStartStateTextBox.Text = new Random().Next(10, 100).ToString();

			if(!string.IsNullOrEmpty(initialDllPath)) {
				this.selectedDllTextBox.Text = initialDllPath;
				this.LoadTypes();
			}

		}


		private void selectDllButton_Click(
			object sender,
			EventArgs e)
		{
			this.dllSelectorDialogBox.ShowDialog();
		}


		private void dllSelectorDialogBox_FileOk(
			object sender,
			CancelEventArgs e)
		{
			this.selectedDllTextBox.Text = this.dllSelectorDialogBox.FileName;
			this.LoadTypes();
		}


		private void LoadTypes()
		{
			this.assemblyInfo = Assembly.LoadFrom(this.selectedDllTextBox.Text);

			Type[] types = this.assemblyInfo.GetTypes();

			this.namespaceFilterTextBox.AutoCompleteCustomSource = Utilities.GetAutoComplete(types);
			this.namespaceFilterTextBox.Text = "";

			this.classNameListingListBox.DataSource = Utilities.GetTypeNames(types, this.namespaceFilterTextBox.Text);
		}


		private void generateButton_Click(
			object sender,
			EventArgs e)
		{
			IEnumerable<string> selectedItems = this.classNameListingListBox.SelectedItems.Cast<string>();

			if (this.nhibernateMappingFilesCheckBox.Checked) {
				NHibernateMappingFileGenerator gen = new NHibernateMappingFileGenerator(this.assemblyInfo);
				gen.UseIdBag = this.useIdBagCheckBox.Checked;
				gen.Execute(selectedItems);
			}

			if (this.nhibernateDaoCheckBox.Checked) {
				new NHibernateDaoFileGenerator(this.assemblyInfo).Execute(selectedItems);
			}

			if (this.daoInterfaceCheckBox.Checked) {
				new DaoInterfaceFileGenerator(this.assemblyInfo).Execute(selectedItems);
			}

			if (this.filterFileCheckBox.Checked) {
				new FilterFileGenerator(this.assemblyInfo).Execute(selectedItems);
				new FilterFillFileGenerator(this.assemblyInfo).Execute(selectedItems);
			}

			if (this.parameterNamesCheckBox.Checked) {
				new ParameterNamesGenerator(this.assemblyInfo).Execute(selectedItems);
			}

			if (this.itemNamesCheckBox.Checked) {
				new ItemNamesGenerator(this.assemblyInfo).Execute(selectedItems);
			}

			if (this.deserializeCheckBox.Checked) {
				new DeserializeGenerator(this.assemblyInfo).Execute(selectedItems);
			}

			if (this.getActionCheckBox.Checked) {
				new GetActionFileGenerator(this.assemblyInfo).Execute(selectedItems);
			}

			if (this.saveActionCheckBox.Checked) {
				new SaveActionFileGenerator(this.assemblyInfo).Execute(selectedItems);
			}

			if (this.createActionCheckBox.Checked) {
				new CreateFromRequestFileGenerator(this.assemblyInfo).Execute(selectedItems);
			}

			if (this.populateActionCheckBox.Checked) {
				new PopulateFromRequestFileGenerator(this.assemblyInfo).Execute(selectedItems);
			}

			if (this.deleteActionCheckBox.Checked) {
				new DeleteActionFileGenerator(this.assemblyInfo).Execute(selectedItems);
			}

			if (this.statesConfigCheckBox.Checked) {
				new StatesConfigFileGenerator(this.assemblyInfo).Execute(selectedItems, int.Parse(this.startStateTextBox.Text));
			}

			if (this.rolesCheckBox.Checked) {
				new RolesCsvFileGenerator(this.assemblyInfo).Execute(selectedItems, int.Parse(this.startStateTextBox.Text));
			}

			if (this.validatorCheckBox.Checked) {
				new ValidatorFileGenerator(this.assemblyInfo).Execute(selectedItems, int.Parse(this.startStateTextBox.Text));
			}

			if (this.pagesCheckBox.Checked) {
				new PagesFileGenerator(this.assemblyInfo).Execute(selectedItems, this.pagesSiteTextBox.Text);
			}

			if (this.nhibernateConfigInsertCheckBox.Checked) {
				new NHibernateConfigInsertGenerator(this.assemblyInfo).Execute(selectedItems);
			}

			if (this.daoFactoryConfigCheckBox.Checked) {
				new DaoConfigGenerator(this.assemblyInfo).Execute(selectedItems);
			}

			if (this.messageCheckBox.Checked) {
				new MessageGenerator(this.assemblyInfo).Execute(selectedItems);
			}

			if (this.mapperCheckBox.Checked) {
				new MessageMapperGenerator(this.assemblyInfo).Execute(selectedItems);
			}

			if (this.serviceImplementationCheckBox.Checked) {
				new ImplemenationGenerator(this.assemblyInfo).Execute(selectedItems);
			}

			if (this.serviceInterfaceCheckBox.Checked) {
				new InterfaceGenerator(this.assemblyInfo).Execute(selectedItems);
			}

			if (this.serviceFilterMessageCheckBox.Checked) {
				new FilterMessageGenerator(this.assemblyInfo).Execute(selectedItems);
			}
		}


		private void Namespace_TextChanged(
			object sender,
			EventArgs e)
		{
			this.classNameListingListBox.DataSource = Utilities.GetTypeNames(this.assemblyInfo.GetTypes(), this.namespaceFilterTextBox.Text);
		}


		private void openOutputButton_Click(
			object sender,
			EventArgs e)
		{
			//avoiding the exception in case the directory is not there, just create it.
			Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + @"\GeneratedFiles\");
			Process.Start(AppDomain.CurrentDomain.BaseDirectory + @"\GeneratedFiles\");
		}


		private void selectAllButton_Click(
			object sender,
			EventArgs e)
		{
			this.nhibernateMappingFilesCheckBox.Checked = true;
			this.pagesCheckBox.Checked = true;
			this.validatorCheckBox.Checked = true;
			this.rolesCheckBox.Checked = true;
			this.statesConfigCheckBox.Checked = true;
			this.deleteActionCheckBox.Checked = true;
			this.populateActionCheckBox.Checked = true;
			this.createActionCheckBox.Checked = true;
			this.saveActionCheckBox.Checked = true;
			this.getActionCheckBox.Checked = true;
			this.deserializeCheckBox.Checked = true;
			this.itemNamesCheckBox.Checked = true;
			this.parameterNamesCheckBox.Checked = true;
			this.filterFileCheckBox.Checked = true;
			this.daoInterfaceCheckBox.Checked = true;
			this.nhibernateDaoCheckBox.Checked = true;
			this.nhibernateMappingFilesCheckBox.Checked = true;
			this.nhibernateConfigInsertCheckBox.Checked = true;
			this.daoFactoryConfigCheckBox.Checked = true;
			this.messageCheckBox.Checked = true;
			this.mapperCheckBox.Checked = true;
			this.serviceInterfaceCheckBox.Checked = true;
			this.serviceFilterMessageCheckBox.Checked = true;
			this.serviceImplementationCheckBox.Checked = true;
			this.serviceStatesCheckBox.Checked = true;
		}


		private void clearAllButton_Click(
			object sender,
			EventArgs e)
		{
			this.nhibernateMappingFilesCheckBox.Checked = false;
			this.pagesCheckBox.Checked = false;
			this.validatorCheckBox.Checked = false;
			this.rolesCheckBox.Checked = false;
			this.statesConfigCheckBox.Checked = false;
			this.deleteActionCheckBox.Checked = false;
			this.populateActionCheckBox.Checked = false;
			this.createActionCheckBox.Checked = false;
			this.saveActionCheckBox.Checked = false;
			this.getActionCheckBox.Checked = false;
			this.deserializeCheckBox.Checked = false;
			this.itemNamesCheckBox.Checked = false;
			this.parameterNamesCheckBox.Checked = false;
			this.filterFileCheckBox.Checked = false;
			this.daoInterfaceCheckBox.Checked = false;
			this.nhibernateDaoCheckBox.Checked = false;
			this.nhibernateMappingFilesCheckBox.Checked = false;
			this.nhibernateConfigInsertCheckBox.Checked = false;
			this.daoFactoryConfigCheckBox.Checked = false;
			this.messageCheckBox.Checked = false;
			this.mapperCheckBox.Checked = false;
			this.serviceInterfaceCheckBox.Checked = false;
			this.serviceFilterMessageCheckBox.Checked = false;
			this.serviceImplementationCheckBox.Checked = false;
			this.serviceStatesCheckBox.Checked = false;
		}


		private void refreshStartStateButton_Click(
			object sender,
			EventArgs e)
		{
			this.startStateTextBox.Text = new Random().Next(10, 100).ToString();
		}


		private void relationshipsButton_Click(
			object sender,
			EventArgs e)
		{
			var dialog = new RelationshipsSelector(this.selectedDllTextBox.Text);
			dialog.ShowDialog();
		}


		private void serviceRefreshButton_Click(
			object sender,
			EventArgs e)
		{
			this.serviceStartStateTextBox.Text = new Random().Next(10, 100).ToString();
		}

		private void registerShellButton_Click(object sender, EventArgs e)
		{
			try {
				// add context menu to the registry
				using (RegistryKey key =
					Registry.ClassesRoot.CreateSubKey(REGISTRY_PATH)) {
					key.SetValue(null, "Generate Code");
				}

				string menuCommand = string.Format("\"{0}\" \"%L\"", Application.ExecutablePath);

				// add command that is invoked to the registry
				using (RegistryKey key = Registry.ClassesRoot.CreateSubKey(
					string.Format(@"{0}\command", REGISTRY_PATH))) {
					key.SetValue(null, menuCommand);
				}
			} catch(Exception ex) {
				
			}
		}

		private void unRegsiterShellButton_Click(object sender, EventArgs e)
		{
			try {
				// remove context menu from the registry
				Registry.ClassesRoot.DeleteSubKeyTree(REGISTRY_PATH);
			} catch {}
		}

		
	}
}