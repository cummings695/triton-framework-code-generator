﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace GenerationSelector
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(args.Length > 0 ? new MainSelector(args[0]) : new MainSelector());
		}
	}
}
