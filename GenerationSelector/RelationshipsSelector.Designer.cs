﻿namespace GenerationSelector
{
	partial class RelationshipsSelector
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RelationshipsSelector));
			this.label1 = new System.Windows.Forms.Label();
			this.parentNamespaceFilterTextBox = new System.Windows.Forms.TextBox();
			this.parentClassNameListingListBox = new System.Windows.Forms.ListBox();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.parentSelectedDllTextBox = new System.Windows.Forms.TextBox();
			this.parentSelectDllButton = new System.Windows.Forms.Button();
			this.selectedClassPropertiesListBox = new System.Windows.Forms.ListBox();
			this.addActionNameTextBox = new System.Windows.Forms.TextBox();
			this.deleteActionNameTextBox = new System.Windows.Forms.TextBox();
			this.generateButton = new System.Windows.Forms.Button();
			this.addActionCheckBox = new System.Windows.Forms.CheckBox();
			this.deleteActionCheckBox = new System.Windows.Forms.CheckBox();
			this.parentOpenFileDialogBox = new System.Windows.Forms.OpenFileDialog();
			this.openOutputButton = new System.Windows.Forms.Button();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(7, 55);
			this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(118, 17);
			this.label1.TabIndex = 16;
			this.label1.Text = "Namespace Filter";
			// 
			// parentNamespaceFilterTextBox
			// 
			this.parentNamespaceFilterTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
			this.parentNamespaceFilterTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
			this.parentNamespaceFilterTextBox.Location = new System.Drawing.Point(133, 52);
			this.parentNamespaceFilterTextBox.Margin = new System.Windows.Forms.Padding(4);
			this.parentNamespaceFilterTextBox.Name = "parentNamespaceFilterTextBox";
			this.parentNamespaceFilterTextBox.Size = new System.Drawing.Size(635, 22);
			this.parentNamespaceFilterTextBox.TabIndex = 15;
			this.parentNamespaceFilterTextBox.TextChanged += new System.EventHandler(this.parentNamespaceFilterTextBox_TextChanged);
			// 
			// parentClassNameListingListBox
			// 
			this.parentClassNameListingListBox.FormattingEnabled = true;
			this.parentClassNameListingListBox.ItemHeight = 16;
			this.parentClassNameListingListBox.Location = new System.Drawing.Point(10, 82);
			this.parentClassNameListingListBox.Margin = new System.Windows.Forms.Padding(4);
			this.parentClassNameListingListBox.Name = "parentClassNameListingListBox";
			this.parentClassNameListingListBox.Size = new System.Drawing.Size(439, 292);
			this.parentClassNameListingListBox.TabIndex = 14;
			this.parentClassNameListingListBox.SelectedIndexChanged += new System.EventHandler(this.parentClassNameListingListBox_SelectedIndexChanged);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.parentSelectedDllTextBox);
			this.groupBox1.Controls.Add(this.parentSelectDllButton);
			this.groupBox1.Controls.Add(this.selectedClassPropertiesListBox);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.parentNamespaceFilterTextBox);
			this.groupBox1.Controls.Add(this.parentClassNameListingListBox);
			this.groupBox1.Location = new System.Drawing.Point(12, 12);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(776, 387);
			this.groupBox1.TabIndex = 18;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Parent Object";
			// 
			// parentSelectedDllTextBox
			// 
			this.parentSelectedDllTextBox.Location = new System.Drawing.Point(10, 22);
			this.parentSelectedDllTextBox.Margin = new System.Windows.Forms.Padding(4);
			this.parentSelectedDllTextBox.Name = "parentSelectedDllTextBox";
			this.parentSelectedDllTextBox.Size = new System.Drawing.Size(628, 22);
			this.parentSelectedDllTextBox.TabIndex = 21;
			// 
			// parentSelectDllButton
			// 
			this.parentSelectDllButton.Location = new System.Drawing.Point(646, 20);
			this.parentSelectDllButton.Margin = new System.Windows.Forms.Padding(4);
			this.parentSelectDllButton.Name = "parentSelectDllButton";
			this.parentSelectDllButton.Size = new System.Drawing.Size(124, 26);
			this.parentSelectDllButton.TabIndex = 20;
			this.parentSelectDllButton.Text = "Select Library";
			this.parentSelectDllButton.UseVisualStyleBackColor = true;
			this.parentSelectDllButton.Click += new System.EventHandler(this.parentSelectDllButton_Click);
			// 
			// selectedClassPropertiesListBox
			// 
			this.selectedClassPropertiesListBox.FormattingEnabled = true;
			this.selectedClassPropertiesListBox.ItemHeight = 16;
			this.selectedClassPropertiesListBox.Location = new System.Drawing.Point(457, 82);
			this.selectedClassPropertiesListBox.Name = "selectedClassPropertiesListBox";
			this.selectedClassPropertiesListBox.Size = new System.Drawing.Size(311, 292);
			this.selectedClassPropertiesListBox.TabIndex = 19;
			this.selectedClassPropertiesListBox.SelectedIndexChanged += new System.EventHandler(this.selectedClassPropertiesListBox_SelectedIndexChanged);
			// 
			// addActionNameTextBox
			// 
			this.addActionNameTextBox.Location = new System.Drawing.Point(179, 407);
			this.addActionNameTextBox.Name = "addActionNameTextBox";
			this.addActionNameTextBox.Size = new System.Drawing.Size(611, 22);
			this.addActionNameTextBox.TabIndex = 20;
			// 
			// deleteActionNameTextBox
			// 
			this.deleteActionNameTextBox.Location = new System.Drawing.Point(179, 436);
			this.deleteActionNameTextBox.Name = "deleteActionNameTextBox";
			this.deleteActionNameTextBox.Size = new System.Drawing.Size(611, 22);
			this.deleteActionNameTextBox.TabIndex = 21;
			// 
			// generateButton
			// 
			this.generateButton.Location = new System.Drawing.Point(236, 469);
			this.generateButton.Name = "generateButton";
			this.generateButton.Size = new System.Drawing.Size(163, 27);
			this.generateButton.TabIndex = 24;
			this.generateButton.Text = "Generate";
			this.generateButton.UseVisualStyleBackColor = true;
			this.generateButton.Click += new System.EventHandler(this.generateButton_Click);
			// 
			// addActionCheckBox
			// 
			this.addActionCheckBox.AutoSize = true;
			this.addActionCheckBox.Checked = true;
			this.addActionCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.addActionCheckBox.Location = new System.Drawing.Point(14, 409);
			this.addActionCheckBox.Name = "addActionCheckBox";
			this.addActionCheckBox.Size = new System.Drawing.Size(139, 21);
			this.addActionCheckBox.TabIndex = 25;
			this.addActionCheckBox.Text = "Add Action Name";
			this.addActionCheckBox.UseVisualStyleBackColor = true;
			this.addActionCheckBox.CheckedChanged += new System.EventHandler(this.addActionCheckBox_CheckedChanged);
			// 
			// deleteActionCheckBox
			// 
			this.deleteActionCheckBox.AutoSize = true;
			this.deleteActionCheckBox.Checked = true;
			this.deleteActionCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.deleteActionCheckBox.Location = new System.Drawing.Point(14, 438);
			this.deleteActionCheckBox.Name = "deleteActionCheckBox";
			this.deleteActionCheckBox.Size = new System.Drawing.Size(155, 21);
			this.deleteActionCheckBox.TabIndex = 26;
			this.deleteActionCheckBox.Text = "Delete Action Name";
			this.deleteActionCheckBox.UseVisualStyleBackColor = true;
			this.deleteActionCheckBox.CheckedChanged += new System.EventHandler(this.deleteActionCheckBox_CheckedChanged);
			// 
			// parentOpenFileDialogBox
			// 
			this.parentOpenFileDialogBox.Filter = "DLLs|*.dll";
			this.parentOpenFileDialogBox.FileOk += new System.ComponentModel.CancelEventHandler(this.parentOpenFileDialogBox_FileOk);
			// 
			// openOutputButton
			// 
			this.openOutputButton.Location = new System.Drawing.Point(405, 469);
			this.openOutputButton.Name = "openOutputButton";
			this.openOutputButton.Size = new System.Drawing.Size(163, 27);
			this.openOutputButton.TabIndex = 27;
			this.openOutputButton.Text = "Open Output";
			this.openOutputButton.UseVisualStyleBackColor = true;
			this.openOutputButton.Click += new System.EventHandler(this.openOutputButton_Click);
			// 
			// RelationshipsSelector
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(800, 507);
			this.Controls.Add(this.openOutputButton);
			this.Controls.Add(this.deleteActionCheckBox);
			this.Controls.Add(this.addActionCheckBox);
			this.Controls.Add(this.generateButton);
			this.Controls.Add(this.deleteActionNameTextBox);
			this.Controls.Add(this.addActionNameTextBox);
			this.Controls.Add(this.groupBox1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.Name = "RelationshipsSelector";
			this.Text = "Relationships Generator";
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox parentNamespaceFilterTextBox;
		private System.Windows.Forms.ListBox parentClassNameListingListBox;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.TextBox addActionNameTextBox;
		private System.Windows.Forms.TextBox deleteActionNameTextBox;
		private System.Windows.Forms.Button generateButton;
		private System.Windows.Forms.CheckBox addActionCheckBox;
		private System.Windows.Forms.CheckBox deleteActionCheckBox;
		private System.Windows.Forms.ListBox selectedClassPropertiesListBox;
		private System.Windows.Forms.TextBox parentSelectedDllTextBox;
		private System.Windows.Forms.Button parentSelectDllButton;
		private System.Windows.Forms.OpenFileDialog parentOpenFileDialogBox;
		private System.Windows.Forms.Button openOutputButton;

	}
}