﻿// COPYRIGHT © 2010 MAD SPROCKET LLC. ALL RIGHTS RESERVED.
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using Core;
using RelationshipGenerator;

namespace GenerationSelector
{
	public partial class RelationshipsSelector : Form
	{
		private Assembly parentAssemblyInfo;


		public RelationshipsSelector(string parentAssemblyPath)
		{
			this.InitializeComponent();

			if (!string.IsNullOrEmpty(parentAssemblyPath)) {
				this.parentSelectedDllTextBox.Text = parentAssemblyPath;
				this.LoadParentTypes();
			}
		}


		private void LoadParentTypes()
		{
			this.parentAssemblyInfo = Assembly.LoadFrom(this.parentSelectedDllTextBox.Text);
			Type[] types = this.parentAssemblyInfo.GetTypes();

			this.parentNamespaceFilterTextBox.AutoCompleteCustomSource = Utilities.GetAutoComplete(types);
			this.parentNamespaceFilterTextBox.Text = "";

			this.parentClassNameListingListBox.DataSource = Utilities.GetTypeNames(types, this.parentNamespaceFilterTextBox.Text);
		}


		private void parentNamespaceFilterTextBox_TextChanged(
			object sender,
			EventArgs e)
		{
			this.parentClassNameListingListBox.DataSource = Utilities.GetTypeNames(this.parentAssemblyInfo.GetTypes(),
			                                                                       this.parentNamespaceFilterTextBox.Text);
		}

		private void PopulateSelectedClassProperties()
		{
			Type fullType = this.parentAssemblyInfo.GetType(this.parentClassNameListingListBox.SelectedItem.ToString());
			List<string> propertyNames = new List<string>();
			foreach (MemberInfo member in fullType.GetMembers()) {
				if (member.MemberType == MemberTypes.Property) {
					Type t = Utilities.GetRealTypeFromMemberProperty(member);
					if (!Utilities.IsPrimitive(t)) {
						string propertyType = Utilities.GetTypeName(t);

						propertyNames.Add(propertyType + " " + member.Name);
					}
				}
			}
			this.selectedClassPropertiesListBox.DataSource = propertyNames;
		}


		private void parentClassNameListingListBox_SelectedIndexChanged(
			object sender,
			EventArgs e)
		{
			this.PopulateSelectedClassProperties();
			this.GenerateActionNames();
		}


		private void GenerateActionNames()
		{
			try {
				string childName = this.GetSelectedPropertyNameForActionName();

				string parentName = this.parentAssemblyInfo.GetType(this.parentClassNameListingListBox.SelectedItem.ToString()).Name;

				this.addActionNameTextBox.Text = string.Format("Add{0}To{1}Action", childName, parentName);
				this.deleteActionNameTextBox.Text = string.Format("Delete{0}From{1}Action", childName, parentName);
			} catch {}
		}


		private string GetSelectedPropertyNameForActionName()
		{
			string selectedItem = this.selectedClassPropertiesListBox.SelectedItem.ToString();

			string name = selectedItem.Split(' ')[1];
			if (selectedItem.StartsWith("IDictionary") || selectedItem.StartsWith("IList")) {
				name = NamingUtilities.Singularize(name);
			}

			return name;
		}


		private string GetSelectedPropertyName()
		{
			string selectedItem = this.selectedClassPropertiesListBox.SelectedItem.ToString();

			return selectedItem.Split(' ')[1];
		}


		private void deleteActionCheckBox_CheckedChanged(
			object sender,
			EventArgs e)
		{
			this.deleteActionNameTextBox.Enabled = this.deleteActionCheckBox.Checked;
		}


		private void addActionCheckBox_CheckedChanged(
			object sender,
			EventArgs e)
		{
			this.addActionNameTextBox.Enabled = this.addActionCheckBox.Checked;
		}


		private void selectedClassPropertiesListBox_SelectedIndexChanged(
			object sender,
			EventArgs e)
		{
			this.GenerateActionNames();
		}

		private void generateButton_Click(
			object sender,
			EventArgs e)
		{
			switch (this.GetActionType()) {
				case "object":
					if (this.addActionCheckBox.Checked) {
						new AddObjectGenerator(this.parentAssemblyInfo).Execute(
							this.parentClassNameListingListBox.SelectedItem.ToString(),
							GetSelectedPropertyChildType(),
							this.addActionNameTextBox.Text,
							this.GetSelectedPropertyName()
							);
					}
					if (this.deleteActionCheckBox.Checked) {
						new DeleteObjectGenerator(this.parentAssemblyInfo).Execute(
							this.parentClassNameListingListBox.SelectedItem.ToString(),
							GetSelectedPropertyChildType(),
							this.deleteActionNameTextBox.Text,
							this.GetSelectedPropertyName()
							);
					}
					break;
				case "simplelist":
					if (this.addActionCheckBox.Checked) {
						new AddSimpleListGenerator(this.parentAssemblyInfo).Execute(
							this.parentClassNameListingListBox.SelectedItem.ToString(),
							GetSelectedPropertyChildType().GetGenericArguments()[0],
							this.addActionNameTextBox.Text,
							this.GetSelectedPropertyName()
							);
					}
					if (this.deleteActionCheckBox.Checked) {
						new DeleteSimpleListGenerator(this.parentAssemblyInfo).Execute(
							this.parentClassNameListingListBox.SelectedItem.ToString(),
							GetSelectedPropertyChildType().GetGenericArguments()[0],
							this.deleteActionNameTextBox.Text,
							this.GetSelectedPropertyName()
							);
					}
					break;
				case "dictionary":
					if (this.addActionCheckBox.Checked) {
						MessageBox.Show("Add Dictionary is not implemented.");
					}
					if (this.deleteActionCheckBox.Checked) {
						MessageBox.Show("Delete Dictionary is not implemented.");
					}
					break;
				case "list":
					if (this.addActionCheckBox.Checked) {
						new AddComplexListGenerator(this.parentAssemblyInfo).Execute(
							this.parentClassNameListingListBox.SelectedItem.ToString(),
							GetSelectedPropertyChildType().GetGenericArguments()[0],
							this.addActionNameTextBox.Text,
							this.GetSelectedPropertyName()
							);
					}
					if (this.deleteActionCheckBox.Checked) {
						new DeleteComplexListGenerator(this.parentAssemblyInfo).Execute(
							this.parentClassNameListingListBox.SelectedItem.ToString(),
							GetSelectedPropertyChildType().GetGenericArguments()[0],
							this.deleteActionNameTextBox.Text,
							this.GetSelectedPropertyName()
							);
					}
					break;
			}
		}

		private Type GetSelectedPropertyChildType()
		{
			string propertyName = this.GetSelectedPropertyName();

			Type selectedChildType = this.parentAssemblyInfo.GetType(this.parentClassNameListingListBox.SelectedItem.ToString());

			MemberInfo[] infos = selectedChildType.GetMember(propertyName);

			return Utilities.GetRealTypeFromMemberProperty(infos[0]);
		}


		private string GetActionType()
		{
			Type t = GetSelectedPropertyChildType();
			string retValue = "object";

			if (t.Name.StartsWith("IList")) {
				retValue = Utilities.IsPrimitive(t.GetGenericArguments()[0]) ? "simplelist" : "list";
			} else if (t.Name.StartsWith("IDictionary")) {
				retValue = "dictionary";
			}  

			return retValue;
		}


		private void parentSelectDllButton_Click(
			object sender,
			EventArgs e)
		{
			this.parentOpenFileDialogBox.ShowDialog();
		}


		private void parentOpenFileDialogBox_FileOk(
			object sender,
			CancelEventArgs e)
		{
			this.parentSelectedDllTextBox.Text = this.parentOpenFileDialogBox.FileName;
			this.LoadParentTypes();
		}


		private void openOutputButton_Click(
			object sender,
			EventArgs e)
		{
			//avoiding the exception in case the directory is not there, just create it.
			Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + @"\GeneratedFiles\");
			Process.Start(AppDomain.CurrentDomain.BaseDirectory + @"\GeneratedFiles\");
		}
	}
}