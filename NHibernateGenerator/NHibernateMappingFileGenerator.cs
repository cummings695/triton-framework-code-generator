﻿// COPYRIGHT © 2010 MAD SPROCKET LLC. ALL RIGHTS RESERVED.
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Core;

namespace NHibernateGenerator
{
	public class NHibernateMappingFileGenerator
	{
		private const string ID_MAPPING =
			@"		<id name=""Id"" column=""`id`"" type=""{TYPE}"">
			<generator class=""{GENERATOR_TYPE}"" />
		</id>";

		private const string MANY_TO_ONE_MAPPING =
			@"		<many-to-one name=""{NAME}"" column=""`{COLUMN}`"" class=""{FULL_CLASS_NAME}"" foreign-key=""{FOREIGN_KEY}""/>";

		private const string NHIBERNATE_DIRECTORY = @"GeneratedFiles\Model\Dao\NHibernate\";

		private const string PROPERTY_MAPPING = @"		<property name=""{NAME}"" column=""`{COLUMN}`"" type=""{TYPE}""/>";

		private const string VERSION_MAPPING = @"		<version name=""Version"" column=""`version`"" type=""{TYPE}"" />";

		private const string ILIST_IDBAG_MAPPING =
			@"		<idbag name=""{NAME}"" table=""`{TABLE_NAME}`"" lazy=""true"">
			<collection-id column=""`id`"" type=""Int64"">
				<generator class=""hilo""/>
			</collection-id>
			<key column=""`{KEY_COLUMN}`"" foreign-key=""{FK_KEY_COLUMN}""/>
			<many-to-many column=""`{OBJECT_COLUMN}`"" class=""{CLASS_NAME}"" foreign-key=""{FK_OBJECT_COLUMN}"" />
		</idbag>";

		private const string ILIST_BAG_MAPPING =
			@"		<bag name=""{NAME}"" table=""`{TABLE_NAME}`"" cascade=""all"" generic=""true"" lazy=""true"">
			<key column=""`{KEY_COLUMN}`"" foreign-key=""{FK_KEY_COLUMN}""/>
			<many-to-many column=""`{OBJECT_COLUMN}`"" class=""{CLASS_NAME}"" foreign-key=""{FK_OBJECT_COLUMN}"" />	
		</bag>";

		private Type currentClassType;

		public NHibernateMappingFileGenerator() {}


		public NHibernateMappingFileGenerator(Assembly assembly)
		{
			this.Assembly = assembly;
		}


		public bool UseIdBag { get; set; }


		public Assembly Assembly { get; set; }


		public void Execute(IEnumerable<string> selectedClassNames)
		{
			//read in the template
			TextReader reader =
				new StreamReader(AppDomain.CurrentDomain.BaseDirectory + @"\TemplateFiles\NHibernate\NHibernateMappingFile.template");

			string template = reader.ReadToEnd();

			TextWriter writer;

			foreach (string fullName in selectedClassNames) {
				//create a new template string
				string classTemplate = template;

				this.currentClassType = this.Assembly.GetType(fullName); //.GetTypeByClassName(className);

				string className = this.currentClassType.Name;
				string assemblyName = this.Assembly.GetName().Name;
				string nameSpace = this.currentClassType.Namespace;
				string tableNameForClass = NamingUtilities.UpdateWithUnderscores(NamingUtilities.Pluralize(className)).ToLower();

				classTemplate = classTemplate.Replace("{ASSEMBLY}", assemblyName);
				classTemplate = classTemplate.Replace("{CLASS_NAME}", className);
				classTemplate = classTemplate.Replace("{NAMESPACE}", nameSpace);
				classTemplate = classTemplate.Replace("{TABLE_NAME_FOR_CLASS}", tableNameForClass);

				classTemplate = this.AddProperties(classTemplate, this.currentClassType);

				Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + NHIBERNATE_DIRECTORY);

				writer = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + NHIBERNATE_DIRECTORY + className + ".hbm.xml");

				writer.Write(classTemplate);

				writer.Flush();
				writer.Close();
			}
		}


		private string AddProperties(
			string classTemplate,
			Type classType)
		{
			string propertiesValue = "";

			//process the ID
			propertiesValue = this.CreateIdNode(classType.GetMember("Id")[0]) + Environment.NewLine;

			//process the version
			if (classType.GetMember("Version") != null && classType.GetMember("Version").Length > 0) {
				propertiesValue = propertiesValue + this.CreateVersionNode(classType.GetMember("Version")[0]) + Environment.NewLine;
			}
			//process the properties
			propertiesValue = propertiesValue + this.CreateProperties(classType.GetMembers()) + Environment.NewLine;

			return classTemplate.Replace("{PROPERTIES}", propertiesValue);
		}


		private string CreateProperties(MemberInfo[] members)
		{
			string properties = "";

			foreach (MemberInfo member in members) {
				if (member.Name != "Id" && member.Name != "Version") {
					properties += this.GetPropertyMapping(member);
				}
			}

			return properties;
		}


		private string GetPropertyMapping(MemberInfo member)
		{
			string propertyMapping = "";

			if (member.MemberType == MemberTypes.Property) {
				Type typeOfMember = Utilities.GetRealTypeFromMemberProperty(member);

				if (Utilities.IsPrimitive(typeOfMember) || Utilities.IsEnum(member)) {
					propertyMapping = this.ProcessPrimitiveTypeProperty(member) + Environment.NewLine;
				} else if (typeOfMember.Name == typeof (IList<object>).Name) {
					propertyMapping += this.ProcessIList(member) + Environment.NewLine;
				} else if (typeOfMember.Name == typeof (IDictionary<object, object>).Name) {
					propertyMapping += "<!-- IDICTIONARY NOT CURRENTLY SUPPORTED FOR " + member.Name + " " + typeOfMember + "-->"
					                   + Environment.NewLine;
				} else {
					//treat as a many to many
					propertyMapping += this.ProcessManyToOne(member) + Environment.NewLine;
				}

				propertyMapping += Environment.NewLine;
			}

			return propertyMapping;
		}


		///<summary>
		///	<idbag name = ""{NAME}"" table = ""{TABLE_NAME}"" lazy = "" true"">
		///		<collection-id column = "" id"" type = "" Int64"">
		///			<generator class = "" hilo"" />
		///		</collection-id>
		///		<key column = ""{KEY_COLUMN}"" foreign-key = ""{FK_KEY_COLUMN}"" />
		///		<many-to-many column = ""{OBJECT_COLUMN}"" class = ""{CLASS_NAME}"" foreign-key = ""{FK_OBJECT_COLUMN}"" />
		///	</idbag>
		///</summary>
		///<param name = "member"></param>
		///<returns></returns>
		private string ProcessIList(MemberInfo member)
		{
			string listMapping = "";

			//Type memberType = this.GetRealTypeFromMemberProperty(member);

			Type memberType = ((PropertyInfo) member).PropertyType.GetGenericArguments()[0];
			if (this.UseIdBag) {
				listMapping = ILIST_IDBAG_MAPPING.Replace("{NAME}", member.Name);
			} else {
				listMapping = ILIST_BAG_MAPPING.Replace("{NAME}", member.Name);
			}

			string tableName;
			if (this.UseIdBag) {
				tableName = NamingUtilities.UpdateWithUnderscores(this.currentClassType.Name).ToLower() + "_" + NamingUtilities.UpdateWithUnderscores(NamingUtilities.Singularize(member.Name)).ToLower()
				            + "_" + NamingUtilities.UpdateWithUnderscores(memberType.Name).ToLower();
			} else {
				tableName = NamingUtilities.UpdateWithUnderscores(this.currentClassType.Name).ToLower() + "_" + NamingUtilities.UpdateWithUnderscores(memberType.Name).ToLower();
			}

			string keyColumn;
			if (this.UseIdBag) {
				keyColumn = NamingUtilities.UpdateWithUnderscores(this.currentClassType.Name).ToLower() + "_" + NamingUtilities.UpdateWithUnderscores(NamingUtilities.Singularize(member.Name)).ToLower() + "_id";
			} else {
				keyColumn = NamingUtilities.UpdateWithUnderscores(this.currentClassType.Name).ToLower() + "_id";
			}
			string fkKeyColumn;
			if (this.UseIdBag) {
				fkKeyColumn = "FK_" + NamingUtilities.UpdateWithUnderscores(this.currentClassType.Name).ToUpper() + "_"
				              + NamingUtilities.UpdateWithUnderscores(NamingUtilities.Singularize(member.Name)).ToUpper() + "_" + NamingUtilities.UpdateWithUnderscores(memberType.Name).ToUpper();
			} else {
				fkKeyColumn = "FK_" + NamingUtilities.UpdateWithUnderscores(this.currentClassType.Name).ToUpper() + "_" + NamingUtilities.UpdateWithUnderscores(memberType.Name).ToUpper();
			}

			string className = string.Format("{0}.{1}, {2}", memberType.Namespace, memberType.Name, memberType.Assembly.GetName().Name);

			string objectColumn = NamingUtilities.UpdateWithUnderscores(memberType.Name).ToLower() + "_id";

			string fkObjectColumn;
			if(this.UseIdBag) {
				fkObjectColumn = "FK_" + NamingUtilities.UpdateWithUnderscores(memberType.Name).ToUpper() + "_" + NamingUtilities.UpdateWithUnderscores(this.currentClassType.Name).ToUpper() + "_"
									+ NamingUtilities.UpdateWithUnderscores(NamingUtilities.Singularize(member.Name)).ToUpper();

			} else {
				fkObjectColumn = "FK_" + NamingUtilities.UpdateWithUnderscores(memberType.Name).ToUpper() + "_" + NamingUtilities.UpdateWithUnderscores(this.currentClassType.Name).ToUpper();

			}
			
			listMapping = listMapping.Replace("{TABLE_NAME}", tableName);
			listMapping = listMapping.Replace("{KEY_COLUMN}", keyColumn);
			listMapping = listMapping.Replace("{FK_KEY_COLUMN}", fkKeyColumn);
			listMapping = listMapping.Replace("{CLASS_NAME}", className);
			listMapping = listMapping.Replace("{OBJECT_COLUMN}", objectColumn);
			listMapping = listMapping.Replace("{FK_OBJECT_COLUMN}", fkObjectColumn);

			return listMapping;
		}


		/// <summary>
		/// 	This is what it will generate
		/// 	<many-to-one name = ""{NAME}"" column = ""{COLUMN}"" class = ""{FULL_CLASS_NAME}"" foreign-key = ""{FOREIGN_KEY}"" />
		/// </summary>
		/// <param name = "member"></param>
		/// <returns></returns>
		private string ProcessManyToOne(MemberInfo member)
		{
			string manyValue = "";

			Type memberType = Utilities.GetRealTypeFromMemberProperty(member);

			manyValue = MANY_TO_ONE_MAPPING.Replace("{NAME}", member.Name);

			manyValue = manyValue.Replace("{COLUMN}", NamingUtilities.UpdateWithUnderscores(member.Name).ToLower() + "_id");

			manyValue = manyValue.Replace("{FULL_CLASS_NAME}",
			                              string.Format("{0}.{1}, {2}",
			                                            memberType.Namespace,
			                                            memberType.Name,
			                                            memberType.Assembly.GetName().Name));

			string foreignKey = string.Format("FK_{0}_{1}",
			                                  NamingUtilities.UpdateWithUnderscores(member.Name).ToUpper(),
			                                  NamingUtilities.UpdateWithUnderscores(member.DeclaringType.Name).ToUpper());

			manyValue = manyValue.Replace("{FOREIGN_KEY}", foreignKey);

			return manyValue;
		}


		/// <summary>
		/// 	generates this: 
		/// 	<property name = ""{NAME}"" column = ""{COLUMN}"" type = ""{TYPE}"" />";
		/// </summary>
		/// <param name = "member"></param>
		/// <returns></returns>
		private string ProcessPrimitiveTypeProperty(MemberInfo member)
		{
			Type memberType = Utilities.GetRealTypeFromMemberProperty(member);

			string propertyValue = PROPERTY_MAPPING.Replace("{NAME}", member.Name);

			propertyValue = propertyValue.Replace("{COLUMN}", NamingUtilities.UpdateWithUnderscores(member.Name).ToLower());

			if (Utilities.IsEnum(member)) {
				propertyValue = propertyValue.Replace("{TYPE}", memberType.FullName + "," + memberType.Assembly.GetName().Name);
			} else {
				propertyValue = propertyValue.Replace("{TYPE}", memberType.FullName);
			}

			return propertyValue;
		}


		private string CreateIdNode(MemberInfo memberInfo)
		{
			string idNode = "";

			Type typeOfId = Utilities.GetRealTypeFromMemberProperty(memberInfo);

			idNode = ID_MAPPING.Replace("{TYPE}", typeOfId.Name);

			string generatorType = "native";

			if (typeOfId == typeof (Guid)) {
				generatorType = "guid";
			}

			idNode = idNode.Replace("{GENERATOR_TYPE}", generatorType);

			return idNode;
		}


		private string CreateVersionNode(MemberInfo memberInfo)
		{
			string versionNode = "";

			Type typeOfVersion = Utilities.GetRealType(((PropertyInfo) memberInfo).PropertyType);

			versionNode = VERSION_MAPPING.Replace("{TYPE}", typeOfVersion.Name);

			return versionNode;
		}
	}
}