using System;
using System.IO;
using System.Reflection;
using Core;

namespace RelationshipGenerator
{
	public class AddObjectGenerator
	{
		private const string DIRECTORY = @"GeneratedFiles\Logic\";


		public AddObjectGenerator() {}


		public AddObjectGenerator(Assembly parent)
		{
			this.ParentAssembly = parent;
		}


		public Assembly ParentAssembly { get; set; }


		public void Execute(
			string parentClass,
			Type childClassType,
			string filename,
			string parentPropertyName)
		{
			//read in the template
			TextReader reader =
				new StreamReader(AppDomain.CurrentDomain.BaseDirectory
								 + @"\TemplateFiles\Relationship\AddObjectToObjectActionFile.template");

			string template = reader.ReadToEnd();

			//create a new template string
			string classTemplate = template;

			Type parentClassType = this.ParentAssembly.GetType(parentClass);
			string parentClassName = parentClassType.Name;

			string childClassName = childClassType.Name;

			string childNamespaceValue = "";

			if(childClassType.Namespace != parentClassType.Namespace) {
				childNamespaceValue = string.Format("using {0};", childClassType.Namespace);
			} 
			classTemplate = classTemplate.Replace("{CHILD_NAMESPACE}", childNamespaceValue);

			classTemplate = classTemplate.Replace("{ACTION_NAME}", filename);

			classTemplate = classTemplate.Replace("{CHILD_CLASS_NAME}", childClassName);
			classTemplate = classTemplate.Replace("{CHILD_CLASS_NAME_CONST}", NamingUtilities.GetConstantName(childClassName));
			classTemplate = classTemplate.Replace("{CHILD_CLASS_NAME_VARIABLE}", NamingUtilities.GetVariableName(childClassName));

			classTemplate = classTemplate.Replace("{PARENT_CLASS_NAME}", parentClassName);
			classTemplate = classTemplate.Replace("{PARENT_CLASS_NAME_CONST}", NamingUtilities.GetConstantName(parentClassName));
			classTemplate = classTemplate.Replace("{PARENT_CLASS_NAME_VARIABLE}", NamingUtilities.GetVariableName(parentClassName));

			classTemplate = classTemplate.Replace("{PARENT_OBJECT_PROPERTY_NAME}", parentPropertyName);
			
			classTemplate = classTemplate.Replace("{MODEL_NAMESPACE}", Utilities.GetModelNameSpace(parentClassType.Namespace));
			classTemplate = classTemplate.Replace("{LOGIC_NAMESPACE}", Utilities.GetLogicNameSpace(parentClassType.Namespace));
			
			classTemplate = classTemplate.Replace("{DAO_NAMESPACE}", Utilities.GetDaoNameSpace(parentClassType.Namespace));
			classTemplate = classTemplate.Replace("{DAO_SUPPORT_NAMESPACE}",
			                                      Utilities.GetDaoSupportNameSpace(parentClassType.Namespace));
			classTemplate = classTemplate.Replace("{SUPPORT_NAMESPACE}", Utilities.GetSupportNameSpace(parentClassType.Namespace));
			classTemplate = classTemplate.Replace("{SUPPORT_REQUEST_NAMESPACE}",
			                                      Utilities.GetSupportRequestNameSpace(parentClassType.Namespace));
			classTemplate = classTemplate.Replace("{NAMESPACE}", Utilities.GetLogicNameSpace(parentClassType.Namespace));

			Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + DIRECTORY);

			TextWriter writer = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + DIRECTORY + filename + ".cs");

			writer.Write(classTemplate);

			writer.Flush();
			writer.Close();
		}
	}
}