using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Core;

namespace DaoGenerator
{
	public class FilterMessageGenerator
	{
		private const string DIRECTORY = @"GeneratedFiles\Service\Message\";


		public FilterMessageGenerator() {}


		public FilterMessageGenerator(Assembly assembly)
		{
			this.Assembly = assembly;
		}


		public Assembly Assembly { get; set; }


		public void Execute(IEnumerable<string> selectedClassNames)
		{
			//read in the template
			TextReader reader = new StreamReader(AppDomain.CurrentDomain.BaseDirectory + @"\TemplateFiles\Services\FilterMessageFile.template");

			string template = reader.ReadToEnd();

			TextWriter writer;

			foreach (string fullName in selectedClassNames) {
				//create a new template string
				string classTemplate = template;

				Type classType = this.Assembly.GetType(fullName);
				string className = classType.Name;

				MemberInfo idMemberInfo = classType.GetMember("Id")[0];
				string idType = Utilities.GetRealTypeFromMemberProperty(idMemberInfo).Name;

				classTemplate = classTemplate.Replace("{CLASS_NAME}", className);
				classTemplate = classTemplate.Replace("{FILTER_MESSAGE_NAMESPACE}", Utilities.GetServiceFilterMessageNameSpace(classType.Namespace));
				classTemplate = classTemplate.Replace("{ID_TYPE}", Utilities.GetCommonNameForType(idType));
				classTemplate = classTemplate.Replace("{MODEL_NAMESPACE}", Utilities.GetModelNameSpace(classType.Namespace));


				Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + DIRECTORY);

				writer = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + DIRECTORY + className + "FilterMessage.cs");

				writer.Write(classTemplate);

				writer.Flush();
				writer.Close();
			}
		}

	}
}