using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Core;

namespace DaoGenerator
{
	public class InterfaceGenerator
	{
		private const string DIRECTORY = @"GeneratedFiles\Service\";


		public InterfaceGenerator() {}


		public InterfaceGenerator(Assembly assembly)
		{
			this.Assembly = assembly;
		}


		public Assembly Assembly { get; set; }


		public void Execute(IEnumerable<string> selectedClassNames)
		{
			//read in the template
			TextReader reader = new StreamReader(AppDomain.CurrentDomain.BaseDirectory + @"\TemplateFiles\Services\InterfaceFile.template");

			string template = reader.ReadToEnd();

			TextWriter writer;

			foreach (string fullName in selectedClassNames) {
				//create a new template string
				string classTemplate = template;

				Type classType = this.Assembly.GetType(fullName);
				string className = classType.Name;

				MemberInfo idMemberInfo = classType.GetMember("Id")[0];
				string idType = Utilities.GetRealTypeFromMemberProperty(idMemberInfo).Name;

				classTemplate = classTemplate.Replace("{CLASS_NAME}", className);
				classTemplate = classTemplate.Replace("{SERVICE_CONTRACT_NAMESPACE}", Utilities.GetServiceNameSpace(classType.Namespace));
				classTemplate = classTemplate.Replace("{MESSAGE_NAMESPACE}", Utilities.GetServiceMessageNameSpace(classType.Namespace));
				classTemplate = classTemplate.Replace("{FILTER_MESSAGE_NAMESPACE}", Utilities.GetServiceFilterMessageNameSpace(classType.Namespace));
				classTemplate = classTemplate.Replace("{ID_TYPE}", Utilities.GetCommonNameForType(idType));
				
				Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + DIRECTORY);

				writer = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + DIRECTORY + "I" + className + "Service.cs");

				writer.Write(classTemplate);

				writer.Flush();
				writer.Close();
			}
		}

	}
}