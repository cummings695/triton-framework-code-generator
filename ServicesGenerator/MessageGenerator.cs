﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Core;

namespace ServicesGenerator
{
	public class MessageGenerator
	{
		private const string DIRECTORY = @"GeneratedFiles\Service\Message\";
		private const string PROPERTY_NODE = @"
		[DataMember]
		public {TYPE} {NAME} { get; set; }";

		public MessageGenerator()
		{
		}

		public MessageGenerator(Assembly assembly)
		{
			this.Assembly = assembly;
		}

		private Type currentClassType;

		public Assembly Assembly { get; set; }


		public void Execute(IEnumerable<string> selectedClassNames)
		{
			//read in the template
			TextReader reader =
				new StreamReader(AppDomain.CurrentDomain.BaseDirectory + @"\TemplateFiles\Services\MessageFile.template");

			string template = reader.ReadToEnd();

			TextWriter writer;

			foreach (string fullName in selectedClassNames) {
				//create a new template string
				string classTemplate = template;

				this.currentClassType = this.Assembly.GetType(fullName); //.GetTypeByClassName(className);

				string className = this.currentClassType.Name;
				string assemblyName = this.Assembly.GetName().Name;
				string nameSpace = Utilities.GetServiceMessageNameSpace(this.currentClassType.Namespace);
				
				classTemplate = classTemplate.Replace("{ASSEMBLY}", assemblyName);
				classTemplate = classTemplate.Replace("{CLASS_NAME}", className);
				classTemplate = classTemplate.Replace("{MESSAGE_NAMESPACE}", nameSpace);
				classTemplate = classTemplate.Replace("{MODEL_NAMESPACE}", Utilities.GetModelNameSpace(this.currentClassType.Namespace));

				classTemplate = this.AddProperties(classTemplate, this.currentClassType);

				Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + DIRECTORY);

				writer = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + DIRECTORY + className + "Message.cs");

				writer.Write(classTemplate);

				writer.Flush();
				writer.Close();
			}
		}


		private string AddProperties(string classTemplate,
									 Type classType)
		{
			string propertiesValue = "";

			//process the ID
			propertiesValue = this.CreateIdNode(classType.GetMember("Id")[0]) + Environment.NewLine;

			//process the properties
			propertiesValue = propertiesValue + this.CreateProperties(classType.GetMembers()) + Environment.NewLine;

			return classTemplate.Replace("{PROPERTIES}", propertiesValue);
		}

		private string CreateProperties(MemberInfo[] members)
		{
			string properties = "";
			foreach (MemberInfo member in members)
			{
				if (member.Name != "Id" && member.Name != "Version")
				{
					properties += this.GetPropertyMapping(member);
				}
			}
			return properties;
		}


		private string GetPropertyMapping(MemberInfo member)
		{
			string propertyMapping = "";

			if (member.MemberType == MemberTypes.Property)
			{
				Type typeOfMember = Utilities.GetRealTypeFromMemberProperty(member);

				string type = Utilities.GetMessageTypeName(typeOfMember);

				type = Utilities.GetCommonNameForType(type);

				propertyMapping = this.CreateNode(type, member.Name);

				propertyMapping += Environment.NewLine;
			}

			return propertyMapping;
		}


		private string CreateIdNode(MemberInfo memberInfo)
		{
			Type typeOfId = Utilities.GetRealTypeFromMemberProperty(memberInfo);

			string type = Utilities.GetCommonNameForType(typeOfId.Name);

			return this.CreateNode(type, "Id");
		}

		private string CreateNode(string type, string name)
		{
			return PROPERTY_NODE.Replace("{TYPE}", type).Replace("{NAME}", name);
		}
	}
}
