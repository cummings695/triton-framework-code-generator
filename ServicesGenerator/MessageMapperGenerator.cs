using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Core;

namespace ServicesGenerator
{
	public class MessageMapperGenerator
	{
		private const string DIRECTORY = @"GeneratedFiles\Service\MessageMapper\";
		
		private const string ENTITY_TO_MESSAGE_PROPERTY_NODE = @"
			entity.{PROPERTY_NAME} = message.{PROPERTY_NAME};";

		private const string MESSAGE_TO_ENTITY_PROPERTY_NODE = @"
			if(entity.{PROPERTY_NAME} != null){
				message.{PROPERTY_NAME} = entity.{PROPERTY_NAME};
			}";

		/*private const string COMPLEX_ENTITY_TO_MESSAGE_PROPERTY_NODE = @"
		entity.{PROPERTY_NAME} = message.{PROPERTY_NAME};"; */

		private const string MESSAGE_TO_COMPLEX_ENTITY_PROPERTY_NODE = @"
			if(entity.{PROPERTY_NAME} != null && entity.{PROPERTY_NAME}.Id != null){
				message.{PROPERTY_NAME} = new {MESSAGE_PROPERTY_TYPE} { Id  = entity.{PROPERTY_NAME}.Id.Value };
			}";

		public MessageMapperGenerator()
		{
		}

		public MessageMapperGenerator(Assembly assembly)
		{
			this.Assembly = assembly;
		}

		private Type currentClassType;

		public Assembly Assembly { get; set; }


		public void Execute(IEnumerable<string> selectedClassNames)
		{
			//read in the template
			TextReader reader =
				new StreamReader(AppDomain.CurrentDomain.BaseDirectory + @"\TemplateFiles\Services\MapperFile.template");

			string template = reader.ReadToEnd();

			TextWriter writer;

			foreach (string fullName in selectedClassNames) {
				//create a new template string
				string classTemplate = template;

				this.currentClassType = this.Assembly.GetType(fullName); //.GetTypeByClassName(className);

				string className = this.currentClassType.Name;
				string assemblyName = this.Assembly.GetName().Name;
				
				classTemplate = classTemplate.Replace("{ASSEMBLY}", assemblyName);
				classTemplate = classTemplate.Replace("{CLASS_NAME}", className);
				classTemplate = classTemplate.Replace("{MESSAGE_MAPPERS_NAMESPACE}", Utilities.GetServiceMapperNameSpace(this.currentClassType.Namespace));
				classTemplate = classTemplate.Replace("{MESSAGE_NAMESPACE}", Utilities.GetServiceMessageNameSpace(this.currentClassType.Namespace));
				classTemplate = classTemplate.Replace("{MODEL_NAMESPACE}", Utilities.GetModelNameSpace(this.currentClassType.Namespace));

				classTemplate = this.AddProperties(classTemplate, this.currentClassType);

				Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + DIRECTORY);

				writer = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + DIRECTORY + className + "MessageMapper.cs");

				writer.Write(classTemplate);

				writer.Flush();
				writer.Close();
			}
		}


		private string AddProperties(string classTemplate,
									 Type classType)
		{
			string entityToMessage = "";
			string messageToEntity = "";

			foreach (MemberInfo member in classType.GetMembers())
			{
				if (member.MemberType == MemberTypes.Property && member.Name != "Version") {
					
					string entToMes = "";
					string mesToEnt = "";
					this.CreateMessage(member, ref entToMes, ref mesToEnt);

					entityToMessage = entityToMessage + entToMes + Environment.NewLine;
					messageToEntity = messageToEntity + mesToEnt + Environment.NewLine;
				}
			}

			return classTemplate.Replace("{ENTITY_TO_MESSAGE}", entityToMessage).Replace("{MESSAGE_TO_ENTITY}", messageToEntity);
		}


		private void CreateMessage(MemberInfo member, ref string entityToMessage, ref string messageToEntity)
		{
			Type typeOfMember = Utilities.GetRealTypeFromMemberProperty(member);

			string type = Utilities.GetMessageTypeName(typeOfMember);

			type = Utilities.GetCommonNameForType(type);

			if (Utilities.IsPrimitive(typeOfMember))
			{
				entityToMessage = ENTITY_TO_MESSAGE_PROPERTY_NODE.Replace("{PROPERTY_NAME}", member.Name);

				messageToEntity = MESSAGE_TO_ENTITY_PROPERTY_NODE.Replace("{PROPERTY_NAME}", member.Name);

			} else if(Utilities.IsList(typeOfMember)){
				
			} else if(Utilities.IsDictionary(typeOfMember)) {
				
			} else
			{
				messageToEntity = MESSAGE_TO_COMPLEX_ENTITY_PROPERTY_NODE.Replace("{PROPERTY_NAME}", member.Name).Replace("{MESSAGE_PROPERTY_TYPE}", type);
			}
		}
	}
}
