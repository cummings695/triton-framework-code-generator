using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Core;

namespace SupportGenerator
{
	public class ItemNamesGenerator
	{
		private const string DAO_SUPPORT_DIRECTORY = @"GeneratedFiles\Support\Request\";


		private const string NESTED_CLASS_TEMPLATE =
			@"		#region Nested type: {CLASS_NAME}

		public class {CLASS_NAME}
		{

			public const string DEFAULT_SEARCH_RESULT_{SEARCH_NAME_UPPER} = ""default_search_result_{SEARCH_NAME}"";
		
		}

		#endregion";

		private const string ITEM_NAMES_CLASS_WRAPPER =
			@"namespace {NAMESPACE}
{
	public class ItemNames
	{
	
{NESTED_CLASSES}

	}
}";


		public ItemNamesGenerator() {}


		public ItemNamesGenerator(Assembly assembly)
		{
			this.Assembly = assembly;
		}


		public Assembly Assembly { get; set; }


		public void Execute(IEnumerable<string> selectedClassNames)
		{
			string nestedClasses = "";

			string nameSpace = "";

			string supportNameSpace = "";

			foreach (string fullName in selectedClassNames) {
				//create a new template string
				string singleClass = NESTED_CLASS_TEMPLATE;

				Type classType = this.Assembly.GetType(fullName);
				string className = classType.Name;

				nameSpace = Utilities.GetDaoSupportNameSpace(classType.Namespace);
				supportNameSpace = Utilities.GetSupportRequestNameSpace(classType.Namespace);

				singleClass = singleClass.Replace("{CLASS_NAME}", className);

				singleClass = singleClass.Replace("{SEARCH_NAME}", NamingUtilities.GetConstantName(className).ToLower());
				singleClass = singleClass.Replace("{SEARCH_NAME_UPPER}", NamingUtilities.GetConstantName(className));
                
				nestedClasses += singleClass + Environment.NewLine + Environment.NewLine;
			}

			string classFile = ITEM_NAMES_CLASS_WRAPPER.Replace("{NESTED_CLASSES}", nestedClasses);

			classFile = classFile.Replace("{NAMESPACE}", supportNameSpace);

			Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + DAO_SUPPORT_DIRECTORY);

			TextWriter writer =
				new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + DAO_SUPPORT_DIRECTORY + "ItemNames.cs");

			writer.Write(classFile);

			writer.Flush();
			writer.Close();
		}

	}
}