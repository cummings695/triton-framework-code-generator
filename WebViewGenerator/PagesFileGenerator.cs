﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using WebViewGenerator.PagesGenerators;

namespace WebViewGenerator
{
	public class PagesFileGenerator
	{
		public PagesFileGenerator(Assembly assembly)
		{
			this.Assembly = assembly;
		}


		public Assembly Assembly { get; set; }


		public void Execute(IEnumerable<string> selectedClassNames, string site)
		{
			new EditCodeBehindFileGenerator(this.Assembly).Execute(selectedClassNames, site);

			new EditPageFileGenerator(this.Assembly).Execute(selectedClassNames, site);

			new ThumbnailsPageFileGenerator(this.Assembly).Execute(selectedClassNames, site);

			new ThumbnailsCodeBehindFileGenerator(this.Assembly).Execute(selectedClassNames, site);

			new LandingPageFileGenerator(this.Assembly).Execute(selectedClassNames, site);

			new LandingCodeBehindFileGenerator(this.Assembly).Execute(selectedClassNames, site);

		}
	}
}
