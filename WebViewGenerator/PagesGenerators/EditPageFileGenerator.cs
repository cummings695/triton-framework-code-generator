using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Core;

namespace WebViewGenerator.PagesGenerators
{
	public class EditPageFileGenerator
	{
		private const string DIRECTORY = @"GeneratedFiles\Web\Pages\v1\{0}\Admin\";

		private const string SINGLE_FIELD = 
			@"				<div class=""form-group"">
				<label for=""{CLASS_NAME_LOWER}_{FIELD_NAME_LOWER}_<%= this.{CLASS_NAME}.Id %>"">{FIELD_NAME_MESSAGE}</label>
				<input id=""{CLASS_NAME_LOWER}_{FIELD_NAME_LOWER}_<%= this.{CLASS_NAME}.Id %>"" value=""<%= this.{CLASS_NAME}.{FIELD_NAME} %>"" class=""required {JS_VALID_TEXT} form-control"" name=""<%= ParameterNames.{CLASS_NAME}.Field.{FIELD_NAME_UPPER} %>"" type=""text"" />
				</div>";

		private string fields = "";

		public EditPageFileGenerator() {}


		public EditPageFileGenerator(Assembly assembly)
		{
			this.Assembly = assembly;
		}


		public Assembly Assembly { get; set; }


		public void Execute(IEnumerable<string> selectedClassNames, string site)
		{
			//read in the template
			TextReader reader = new StreamReader(AppDomain.CurrentDomain.BaseDirectory + @"\TemplateFiles\Pages\EditPageFile.template");

			string template = reader.ReadToEnd();

			TextWriter writer;

			foreach (string fullName in selectedClassNames) {
				//create a new template string
				string classTemplate = template;

				this.fields = "";

				Type classType = this.Assembly.GetType(fullName);
				string className = classType.Name;


				classTemplate = classTemplate.Replace("{SITE}", site);

				classTemplate = classTemplate.Replace("{CLASS_NAME}", className);
				//classTemplate = classTemplate.Replace("{CLASS_NAME_UPPER}", className.ToUpper());
				classTemplate = classTemplate.Replace("{CLASS_NAME_LOWER}", NamingUtilities.GetConstantName(className).ToLower());
				classTemplate = classTemplate.Replace("{CLASS_NAME_MESSAGE}", NamingUtilities.GetStringName(className));


				classTemplate = classTemplate.Replace("{MODEL_NAMESPACE}", Utilities.GetModelNameSpace(classType.Namespace));
				classTemplate = classTemplate.Replace("{DAO_NAMESPACE}", Utilities.GetDaoNameSpace(classType.Namespace));
				classTemplate = classTemplate.Replace("{DAO_SUPPORT_NAMESPACE}", Utilities.GetDaoSupportNameSpace(classType.Namespace));
				classTemplate = classTemplate.Replace("{SUPPORT_NAMESPACE}", Utilities.GetSupportNameSpace(classType.Namespace));
				classTemplate = classTemplate.Replace("{SUPPORT_REQUEST_NAMESPACE}", Utilities.GetSupportRequestNameSpace(classType.Namespace));
				classTemplate = classTemplate.Replace("{NAMESPACE}", Utilities.GetLogicNameSpace(classType.Namespace));

				this.ProcessFields(className, classType);

				classTemplate = classTemplate.Replace("{FIELDS}", this.fields);

				string directory = AppDomain.CurrentDomain.BaseDirectory + string.Format(DIRECTORY, site) + className;

				Directory.CreateDirectory(directory);

				writer = new StreamWriter(directory + "\\Edit.aspx");

				writer.Write(classTemplate);

				writer.Flush();
				writer.Close();
			}
		}


		private void ProcessFields(string className,
								   Type classType)
		{

			foreach (MemberInfo member in classType.GetMembers()) {
				if (member.MemberType == MemberTypes.Property) {
					if (member.Name != "Id" && member.Name != "Version") {
						Type t = Utilities.GetRealTypeFromMemberProperty(member);

						if (Utilities.IsPrimitive(t)) {
							this.fields += SINGLE_FIELD.Replace("{CLASS_NAME}", className)
							               	.Replace("{CLASS_NAME_LOWER}", NamingUtilities.GetConstantName(className).ToLower())
							               	.Replace("{FIELD_NAME}", member.Name)
											.Replace("{FIELD_NAME_MESSAGE}", NamingUtilities.GetStringName(member.Name))
							               	.Replace("{FIELD_NAME_UPPER}", NamingUtilities.GetConstantName(member.Name))
							               	.Replace("{FIELD_NAME_LOWER}", NamingUtilities.GetConstantName(member.Name).ToLower())
							               	.Replace("{JS_VALID_TEXT}", "") + Environment.NewLine;

						}
					}
				}
			}
		}
	}
}