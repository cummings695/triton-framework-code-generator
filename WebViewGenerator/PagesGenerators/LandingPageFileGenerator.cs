using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Core;

namespace WebViewGenerator.PagesGenerators
{
	public class LandingPageFileGenerator
	{
		private const string DIRECTORY = @"GeneratedFiles\Web\Pages\v1\{0}\Admin\";


		public LandingPageFileGenerator() {}


		public LandingPageFileGenerator(Assembly assembly)
		{
			this.Assembly = assembly;
		}


		public Assembly Assembly { get; set; }


		public void Execute(IEnumerable<string> selectedClassNames, string site)
		{
			//read in the template
			TextReader reader = new StreamReader(AppDomain.CurrentDomain.BaseDirectory + @"\TemplateFiles\Pages\LandingPageFile.template");

			string template = reader.ReadToEnd();

			TextWriter writer;

			foreach (string fullName in selectedClassNames) {
				//create a new template string
				string classTemplate = template;

				Type classType = this.Assembly.GetType(fullName);
				string className = classType.Name;

				classTemplate = classTemplate.Replace("{SITE}", site);

				classTemplate = classTemplate.Replace("{CLASS_NAME}", className);
				classTemplate = classTemplate.Replace("{CLASS_NAME_MESSAGE}", NamingUtilities.GetStringName(className));
				classTemplate = classTemplate.Replace("{CLASS_NAME_LOWER}", NamingUtilities.GetVariableName(className));

				classTemplate = classTemplate.Replace("{MODEL_NAMESPACE}", Utilities.GetModelNameSpace(classType.Namespace));
				classTemplate = classTemplate.Replace("{DAO_NAMESPACE}", Utilities.GetDaoNameSpace(classType.Namespace));
				classTemplate = classTemplate.Replace("{DAO_SUPPORT_NAMESPACE}", Utilities.GetDaoSupportNameSpace(classType.Namespace));
				classTemplate = classTemplate.Replace("{SUPPORT_NAMESPACE}", Utilities.GetSupportNameSpace(classType.Namespace));
				classTemplate = classTemplate.Replace("{SUPPORT_REQUEST_NAMESPACE}", Utilities.GetSupportRequestNameSpace(classType.Namespace));
				classTemplate = classTemplate.Replace("{NAMESPACE}", Utilities.GetLogicNameSpace(classType.Namespace));

				string directory = AppDomain.CurrentDomain.BaseDirectory + string.Format(DIRECTORY, site) + className;

				Directory.CreateDirectory(directory);

				writer = new StreamWriter(directory + "\\Landing.aspx");

				writer.Write(classTemplate);

				writer.Flush();
				writer.Close();
			}
		}

	}
}