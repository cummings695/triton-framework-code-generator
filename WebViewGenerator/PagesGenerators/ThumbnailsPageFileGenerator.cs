using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Core;

namespace WebViewGenerator.PagesGenerators
{
	public class ThumbnailsPageFileGenerator
	{
		private const string DIRECTORY = @"GeneratedFiles\Web\Pages\v1\{0}\Admin\";

		private const string EVAL_FIELD = @"					<td><%# Eval(""{0}"") %></td>";
		private const string HEADER_FIELD = @"						<th>{0}</th>";

		public const int MAX_FIELD_COUNT = 3;

		private string evalFields = "";
		private string headerFields = "";


		public ThumbnailsPageFileGenerator() {}


		public ThumbnailsPageFileGenerator(Assembly assembly)
		{
			this.Assembly = assembly;
		}


		public Assembly Assembly { get; set; }


		public void Execute(IEnumerable<string> selectedClassNames, string site)
		{
			//read in the template
			TextReader reader = new StreamReader(AppDomain.CurrentDomain.BaseDirectory + @"\TemplateFiles\Pages\ThumbnailsPageFile.template");

			string template = reader.ReadToEnd();

			TextWriter writer;

			foreach (string fullName in selectedClassNames) {
				//create a new template string
				string classTemplate = template;

				Type classType = this.Assembly.GetType(fullName);
				string className = classType.Name;

				classTemplate = classTemplate.Replace("{SITE}", site);

				classTemplate = classTemplate.Replace("{CLASS_NAME}", className);
				classTemplate = classTemplate.Replace("{CLASS_NAME_UPPER}", NamingUtilities.GetConstantName(className));
				classTemplate = classTemplate.Replace("{CLASS_NAME_LOWER}", NamingUtilities.GetVariableName(className));
				classTemplate = classTemplate.Replace("{CLASS_NAME_CONSTANT_LOWER}", NamingUtilities.GetConstantName(className).ToLower());
				classTemplate = classTemplate.Replace("{CLASS_NAME_MESSAGE}", NamingUtilities.GetStringName(className));

				classTemplate = classTemplate.Replace("{MODEL_NAMESPACE}", Utilities.GetModelNameSpace(classType.Namespace));
				classTemplate = classTemplate.Replace("{DAO_NAMESPACE}", Utilities.GetDaoNameSpace(classType.Namespace));
				classTemplate = classTemplate.Replace("{DAO_SUPPORT_NAMESPACE}", Utilities.GetDaoSupportNameSpace(classType.Namespace));
				classTemplate = classTemplate.Replace("{SUPPORT_NAMESPACE}", Utilities.GetSupportNameSpace(classType.Namespace));
				classTemplate = classTemplate.Replace("{SUPPORT_REQUEST_NAMESPACE}", Utilities.GetSupportRequestNameSpace(classType.Namespace));
				classTemplate = classTemplate.Replace("{NAMESPACE}", Utilities.GetLogicNameSpace(classType.Namespace));

				this.evalFields = "";
				this.headerFields = "";

				this.GetFields(classType);

				classTemplate = classTemplate.Replace("{EVAL_FIELDS}", this.evalFields);
				classTemplate = classTemplate.Replace("{HEADER_FIELDS}", this.headerFields);

				string directory = AppDomain.CurrentDomain.BaseDirectory + string.Format(DIRECTORY, site) + className;

				Directory.CreateDirectory(directory);

				writer = new StreamWriter(directory + "\\Thumbnails.aspx");

				writer.Write(classTemplate);

				writer.Flush();
				writer.Close();
			}
		}


		private void GetFields(Type type)
		{
			int count = 0;
			foreach (MemberInfo member in type.GetMembers()) {
				if (count > MAX_FIELD_COUNT) {
					break;
				}

				if (member.MemberType == MemberTypes.Property) {
					if (member.Name != "Id" && member.Name != "Version") {
						Type t = Utilities.GetRealTypeFromMemberProperty(member);

						if (Utilities.IsPrimitive(t)) {
							this.headerFields += string.Format(HEADER_FIELD, NamingUtilities.GetStringName(member.Name)) + Environment.NewLine;
							this.evalFields += string.Format(EVAL_FIELD, member.Name) + Environment.NewLine;

							count++;
						}
					}
				}
			}
		}
	}
}