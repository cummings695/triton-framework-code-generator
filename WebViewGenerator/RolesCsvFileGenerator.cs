using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Core;

namespace WebViewGenerator
{
	public class RolesCsvFileGenerator
	{
		private const string DIRECTORY = @"GeneratedFiles\Web\Utilities\";


		public RolesCsvFileGenerator() {}


		public RolesCsvFileGenerator(Assembly assembly)
		{
			this.Assembly = assembly;
		}


		public Assembly Assembly { get; set; }


		public void Execute(IEnumerable<string> selectedClassNames, int prependStateNumberStart)
		{
			//read in the template
			TextReader reader = new StreamReader(AppDomain.CurrentDomain.BaseDirectory + @"\TemplateFiles\Web\RolesCsv.template");

			string template = reader.ReadToEnd();

			Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + DIRECTORY);

			TextWriter writer = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + DIRECTORY + "Roles" + prependStateNumberStart + ".csv");

			foreach (string fullName in selectedClassNames) {
				//create a new template string
				string classTemplate = template;

				Type classType = this.Assembly.GetType(fullName);
				string className = classType.Name;

				classTemplate = classTemplate.Replace("{CLASS_NAME}", className);
				classTemplate = classTemplate.Replace("{CLASS_NAME_LOWER}", NamingUtilities.GetStringName(className));
                
				writer.Write(classTemplate + Environment.NewLine);
			}
			
			writer.Flush();
			writer.Close();
		}

	}
}