using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Core;

namespace WebViewGenerator
{
	public class StatesConfigFileGenerator
	{
		private const string DIRECTORY = @"GeneratedFiles\Web\States\";


		public StatesConfigFileGenerator() {}


		public StatesConfigFileGenerator(Assembly assembly)
		{
			this.Assembly = assembly;
		}


		public Assembly Assembly { get; set; }


		public void Execute(IEnumerable<string> selectedClassNames, int prependStateNumberStart)
		{
			//read in the template
			TextReader reader = new StreamReader(AppDomain.CurrentDomain.BaseDirectory + @"\TemplateFiles\Web\StatesGraph.graphml");

			string template = reader.ReadToEnd();

			TextWriter writer;

			int prependStateNumber = prependStateNumberStart;

			foreach (string fullName in selectedClassNames) {
				//create a new template string
				string classTemplate = template;

				Type classType = this.Assembly.GetType(fullName);
				string className = classType.Name;

				classTemplate = classTemplate.Replace("{CLASS_NAME}", className);

				classTemplate = classTemplate.Replace("{PREPEND_STATE}", prependStateNumber.ToString());
				
				Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + DIRECTORY);

				writer = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + DIRECTORY + "Admin." + prependStateNumber + "000." + className + ".graphml");

				writer.Write(classTemplate);

				writer.Flush();
				writer.Close();

				prependStateNumber++;
			}
		}
	}
}